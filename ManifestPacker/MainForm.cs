﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ManifestPacker
{
    public partial class MainForm : Form
    {
        TextPrompt myTextPrompt;

        List<GroupBox> myContentGroups;

        public MainForm()
        {
            InitializeComponent();

            myTextPrompt = new TextPrompt("prompt");

            trvPackage.NodeMouseClick += NodeClicked;

            myContentGroups = new List<GroupBox>();

            myContentGroups.Add(grpTexture);
            myContentGroups.Add(grpShader);
            myContentGroups.Add(grpCubeMap);
            myContentGroups.Add(grpSoundEffect);
            myContentGroups.Add(grpModel);
        }

        private void NodeClicked(object sender, TreeNodeMouseClickEventArgs e)
        {
            trvPackage.SelectedNode = e.Node;

            Type nodeType = e.Node.Tag.GetType();

            if (e.Button == MouseButtons.Right) {
                if (e.Node.Name == "tvnPackage") {
                    ShowRootNodeContextMenu(e);
                }
                else if (e.Node.Tag.GetType() == typeof(Bundle)){
                    ShowBundleContextMenu(e);
                }
                else if (e.Node.Tag.GetType().Name == "RuntimeType") {
                    ShowCollectionContextMenu(e);
                }
                else {
                    ShowItemMenu(e);
                }
            }
            else if (e.Button == MouseButtons.Left) {
                if (e.Node.Name == "tvnPackage") {
                    // Root left click
                }
                else if (e.Node.Tag.GetType() == typeof(Bundle)) {
                    // Bundle left click
                }
                else {
                    ShowContentView(e.Node);
                }
            }
        }

        private void ShowRootNodeContextMenu(TreeNodeMouseClickEventArgs e)
        {
            cmsPackage.Tag = e.Node;
            cmsPackage.Show(PointToScreen(e.Location));
        }

        private void ShowCollectionContextMenu(TreeNodeMouseClickEventArgs e)
        {
            cmsCollection.Tag = e.Node;
            cmsCollection.Show(PointToScreen(e.Location));
        }

        private void ShowBundleContextMenu(TreeNodeMouseClickEventArgs e) {
            cmsBundle.Tag = e.Node;
            cmsBundle.Show(PointToScreen(e.Location));
        }

        private void ShowItemMenu(TreeNodeMouseClickEventArgs e)
        {
            cmsItemMenu.Tag = e.Node;
            cmsItemMenu.Show(PointToScreen(e.Location));
        }

        private void ShowContentView(TreeNode node) {
            Type t = node.Tag.GetType();

            if (t == typeof(TextureItem)) {
                TextureItem item = (TextureItem)node.Tag;

                for(int ix = 0; ix < myContentGroups.Count; ix++) {
                    myContentGroups[ix].Visible = false;
                }

                grpTexture.Visible = true;
                ctrlTexture.Item = item;
            }
            else if (t == typeof(ShaderItem))
            {
                ShaderItem item = (ShaderItem)node.Tag;

                for (int ix = 0; ix < myContentGroups.Count; ix++)
                {
                    myContentGroups[ix].Visible = false;
                }

                grpShader.Visible = true;
                ctrlShaderConfig.Item = item;
            }
            else if (t == typeof(CubeMapItem))
            {
                CubeMapItem item = (CubeMapItem)node.Tag;

                for (int ix = 0; ix < myContentGroups.Count; ix++)
                {
                    myContentGroups[ix].Visible = false;
                }

                grpCubeMap.Visible = true;
                ctrlCubeMap.Item = item;
            }
            else if (t == typeof(SoundEffectItem))
            {
                SoundEffectItem item = (SoundEffectItem)node.Tag;

                for (int ix = 0; ix < myContentGroups.Count; ix++)
                {
                    myContentGroups[ix].Visible = false;
                }

                grpSoundEffect.Visible = true;
                ctrlSoundConfig.Item = item;
            }
            else if (t == typeof(ModelItem))
            {
                ModelItem item = (ModelItem)node.Tag;

                for (int ix = 0; ix < myContentGroups.Count; ix++)
                {
                    myContentGroups[ix].Visible = false;
                }

                grpModel.Visible = true;
                ctrlModel.Item = item;
            }
            else
            {
                for (int ix = 0; ix < myContentGroups.Count; ix++)
                {
                    myContentGroups[ix].Visible = false;
                }
            }
        }
        
        private void AddBundleClicked(object sender, EventArgs e) {
            myTextPrompt.Prompt = "Enter bundle name";
            myTextPrompt.ResultText = "";
            DialogResult result = myTextPrompt.ShowDialog();

            if (result == DialogResult.OK && myTextPrompt.ResultText != "") {
                if (trvPackage.Nodes[0].Nodes.Find(myTextPrompt.ResultText, false).Length != 0) {
                    MessageBox.Show("Duplicate name found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                Bundle bundle = new Bundle();

                TreeNode node = new TreeNode(myTextPrompt.ResultText);
                node.Name = myTextPrompt.ResultText;
                node.Tag = bundle;

                TreeNode textureNode = new TreeNode("Textures");
                textureNode.Tag = typeof(TextureItem);
                node.Nodes.Add(textureNode);

                TreeNode shaderNode = new TreeNode("Shaders");
                shaderNode.Tag = typeof(ShaderItem);
                node.Nodes.Add(shaderNode);

                TreeNode modelNode = new TreeNode("Models");
                modelNode.Tag = typeof(ModelItem);
                node.Nodes.Add(modelNode);

                TreeNode cubeMapNode = new TreeNode("CubeMaps");
                cubeMapNode.Tag = typeof(CubeMapItem);
                node.Nodes.Add(cubeMapNode);

                TreeNode soundEffectNode = new TreeNode("Sound Effects");
                soundEffectNode.Tag = typeof(SoundEffectItem);
                node.Nodes.Add(soundEffectNode);

                trvPackage.Nodes[0].Nodes.Add(node);
                trvPackage.Nodes[0].Expand();

                node.Expand();
            }
        }

        private void AddCollectionItemClicked(object sender, EventArgs e) {

            Type t = e.GetType();
            string name;

            TreeNode treeNode = (TreeNode)(cmsCollection.Tag);

            myTextPrompt.Prompt = "Enter item name";
            myTextPrompt.ResultText = "";
            DialogResult result = myTextPrompt.ShowDialog();

            if (result == DialogResult.OK)
            {
                name = myTextPrompt.ResultText;

                Object item = Activator.CreateInstance((Type)treeNode.Tag, name);

                Bundle bundle = (Bundle)treeNode.Parent.Tag;

                bundle.AddTypelessResource(item);

                TreeNode node = new TreeNode(name);
                node.Tag = item;

                treeNode.Nodes.Add(node);
                treeNode.Expand();
            }
        }

        private void AddBundleItem(TreeNode bundleNode, Type itemType) {
            TreeNode target = null;

            for(int ix = 0; ix < bundleNode.Nodes.Count; ix ++) {
                if ((Type)bundleNode.Nodes[ix].Tag == itemType) {
                    target = bundleNode.Nodes[ix];
                    break;
                }
            }

            if (target != null)
            {
                myTextPrompt.Prompt = "Enter item name";
                myTextPrompt.ResultText = "";
                DialogResult result = myTextPrompt.ShowDialog();


                if (trvPackage.Nodes.Find(myTextPrompt.ResultText, false).Length != 0) {
                    MessageBox.Show("Duplicate name detected");
                    return;
                }

                if (result == DialogResult.OK)
                {
                    string name = myTextPrompt.ResultText;

                    Object item = Activator.CreateInstance(itemType, name);

                    TreeNode node = new TreeNode(name);
                    node.Tag = item;

                    target.Nodes.Add(node);
                    target.Expand();
                }
            }
        }

        private void tsiLoad_Click(object sender, EventArgs e)
        {
            DialogResult results = dlgOpenManifest.ShowDialog();

            if (results == DialogResult.OK && File.Exists(dlgOpenManifest.FileName))
            {
                List<Bundle> bundles = new List<Bundle>();
                trvPackage.Nodes[0].Nodes.Clear();

                // TODO: unload current package
                Settings.WorkingDirectory = Directory.GetParent(Path.GetDirectoryName(dlgOpenManifest.FileName)).FullName;
                Settings.WorkingDirectory = Settings.WorkingDirectory.Replace('\\', '/');

                ManifestLoader.LoadManifest(dlgOpenManifest.FileName, bundles, trvPackage);
                Settings.WorkingFileName = dlgSaveManifest.FileName;
            }
        }

        private void tsiSave_Click(object sender, EventArgs e)
        {
            if (Settings.WorkingFileName == null)
            {
                DialogResult result = dlgSaveManifest.ShowDialog();

                if (result != DialogResult.OK)
                    return;

                Settings.WorkingFileName = dlgSaveManifest.FileName;
            }

            ManifestLoader.SaveManifest(Settings.WorkingFileName, trvPackage.Nodes[0]);
        }

        private void tsiSaveAs_Click(object sender, EventArgs e)
        {
            DialogResult result = dlgSaveManifest.ShowDialog();

            if (result != DialogResult.OK)
                return;

            Settings.WorkingFileName = dlgSaveManifest.FileName;

            ManifestLoader.SaveManifest(Settings.WorkingFileName, trvPackage.Nodes[0]);
        }

        private void tsiNew_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to create a new project?", "New Project", MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes) {
                trvPackage.Nodes[0].Nodes.Clear();
                Settings.WorkingFileName = null;

                for (int ix = 0; ix < myContentGroups.Count; ix++) {
                    myContentGroups[ix].Visible = false;
                }
            }
        }

        private void ValidateLabelEdit(object sender, NodeLabelEditEventArgs e) {
            // TODO search for duplicates
            if (e.Node.Parent.Nodes.Find(e.Label, false).Length != 0) {
                e.CancelEdit = true;
                MessageBox.Show("Duplicate name detected");
            }
        }

        private void ValidateCanEditLabel(object sender, NodeLabelEditEventArgs e)
        {
            if (e.Node == trvPackage.Nodes[0])
                e.CancelEdit = true;

            if (e.Node.Tag != null && e.Node.Tag.GetType() == typeof(Type))
                e.CancelEdit = true;
        }

        private void BundleRename(object sender, EventArgs e) {
            TreeNode node = (TreeNode)cmsBundle.Tag;
            node.BeginEdit();
        }

        private void ItemRenameClicked(object sender, EventArgs e) {
            TreeNode node = (TreeNode)cmsItemMenu.Tag;
            node.BeginEdit();
        }

        private void ItemDeleteClick(object sender, EventArgs e)
        {
            TreeNode node = (TreeNode)cmsItemMenu.Tag;
            node.BeginEdit();

            DialogResult dialogResult = MessageBox.Show("Are you sure you want to delete this item?", "Delete?", MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                Bundle bundle = (Bundle)node.Parent.Parent.Tag;
                bundle.RemoveGeneric(node.Tag);
                node.Remove();                
            }
        }
    }
}