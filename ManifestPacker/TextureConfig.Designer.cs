﻿namespace ManifestPacker
{
    partial class TextureConfig
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbWrap = new System.Windows.Forms.ComboBox();
            this.cmbSampleState = new System.Windows.Forms.ComboBox();
            this.lblWrapMode = new System.Windows.Forms.Label();
            this.lblSampling = new System.Windows.Forms.Label();
            this.chkAniso = new System.Windows.Forms.CheckBox();
            this.ctrlSource = new ManifestPacker.FileConfigItem();
            this.SuspendLayout();
            // 
            // cmbWrap
            // 
            this.cmbWrap.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWrap.FormattingEnabled = true;
            this.cmbWrap.Location = new System.Drawing.Point(86, 31);
            this.cmbWrap.Name = "cmbWrap";
            this.cmbWrap.Size = new System.Drawing.Size(215, 21);
            this.cmbWrap.TabIndex = 2;
            // 
            // cmbSampleState
            // 
            this.cmbSampleState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSampleState.FormattingEnabled = true;
            this.cmbSampleState.Location = new System.Drawing.Point(86, 58);
            this.cmbSampleState.Name = "cmbSampleState";
            this.cmbSampleState.Size = new System.Drawing.Size(215, 21);
            this.cmbSampleState.TabIndex = 3;
            // 
            // lblWrapMode
            // 
            this.lblWrapMode.AutoSize = true;
            this.lblWrapMode.Location = new System.Drawing.Point(14, 34);
            this.lblWrapMode.Name = "lblWrapMode";
            this.lblWrapMode.Size = new System.Drawing.Size(66, 13);
            this.lblWrapMode.TabIndex = 4;
            this.lblWrapMode.Text = "Wrap Mode:";
            // 
            // lblSampling
            // 
            this.lblSampling.AutoSize = true;
            this.lblSampling.Location = new System.Drawing.Point(27, 61);
            this.lblSampling.Name = "lblSampling";
            this.lblSampling.Size = new System.Drawing.Size(53, 13);
            this.lblSampling.TabIndex = 5;
            this.lblSampling.Text = "Sampling:";
            // 
            // chkAniso
            // 
            this.chkAniso.AutoSize = true;
            this.chkAniso.Location = new System.Drawing.Point(86, 85);
            this.chkAniso.Name = "chkAniso";
            this.chkAniso.Size = new System.Drawing.Size(75, 17);
            this.chkAniso.TabIndex = 7;
            this.chkAniso.Text = "Anisotropy";
            this.chkAniso.UseVisualStyleBackColor = true;
            // 
            // ctrlSource
            // 
            this.ctrlSource.DefaultExt = "";
            this.ctrlSource.Dock = System.Windows.Forms.DockStyle.Top;
            this.ctrlSource.FileFilter = "All|*.png;*.gif;*.jpg;*.jpeg|PNG|*.png|JPEG|*.jpg;*.jpeg|GIF|*.gif";
            this.ctrlSource.FileName = "";
            this.ctrlSource.LabelText = "Source:";
            this.ctrlSource.Location = new System.Drawing.Point(0, 0);
            this.ctrlSource.Name = "ctrlSource";
            this.ctrlSource.Size = new System.Drawing.Size(362, 30);
            this.ctrlSource.TabIndex = 8;
            // 
            // TextureConfig
            // 
            this.Controls.Add(this.ctrlSource);
            this.Controls.Add(this.chkAniso);
            this.Controls.Add(this.lblSampling);
            this.Controls.Add(this.lblWrapMode);
            this.Controls.Add(this.cmbSampleState);
            this.Controls.Add(this.cmbWrap);
            this.Name = "TextureConfig";
            this.Size = new System.Drawing.Size(362, 119);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cmbWrap;
        private System.Windows.Forms.ComboBox cmbSampleState;
        private System.Windows.Forms.Label lblWrapMode;
        private System.Windows.Forms.Label lblSampling;
        private System.Windows.Forms.CheckBox chkAniso;
        private FileConfigItem ctrlSource;
    }
}
