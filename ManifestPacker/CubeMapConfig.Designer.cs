﻿namespace ManifestPacker
{
    partial class CubeMapConfig
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ctrlNegX = new ManifestPacker.FileConfigItem();
            this.ctrlPosX = new ManifestPacker.FileConfigItem();
            this.ctrlNegY = new ManifestPacker.FileConfigItem();
            this.ctrlPosY = new ManifestPacker.FileConfigItem();
            this.ctrlNegZ = new ManifestPacker.FileConfigItem();
            this.ctrlPosZ = new ManifestPacker.FileConfigItem();
            this.SuspendLayout();
            // 
            // ctrlNegX
            // 
            this.ctrlNegX.DefaultExt = "";
            this.ctrlNegX.Dock = System.Windows.Forms.DockStyle.Top;
            this.ctrlNegX.FileFilter = "All|*.png;*.gif;*.jpg;*.jpeg|PNG|*.png|JPEG|*.jpg;*.jpeg|GIF|*.gif";
            this.ctrlNegX.FileName = "";
            this.ctrlNegX.LabelText = "-X:";
            this.ctrlNegX.Location = new System.Drawing.Point(0, 0);
            this.ctrlNegX.Name = "ctrlNegX";
            this.ctrlNegX.Size = new System.Drawing.Size(464, 30);
            this.ctrlNegX.TabIndex = 0;
            // 
            // ctrlPosX
            // 
            this.ctrlPosX.DefaultExt = "";
            this.ctrlPosX.Dock = System.Windows.Forms.DockStyle.Top;
            this.ctrlPosX.FileFilter = "All|*.png;*.gif;*.jpg;*.jpeg|PNG|*.png|JPEG|*.jpg;*.jpeg|GIF|*.gif";
            this.ctrlPosX.FileName = "";
            this.ctrlPosX.LabelText = "+X:";
            this.ctrlPosX.Location = new System.Drawing.Point(0, 30);
            this.ctrlPosX.Name = "ctrlPosX";
            this.ctrlPosX.Size = new System.Drawing.Size(464, 30);
            this.ctrlPosX.TabIndex = 1;
            // 
            // ctrlNegY
            // 
            this.ctrlNegY.DefaultExt = "";
            this.ctrlNegY.Dock = System.Windows.Forms.DockStyle.Top;
            this.ctrlNegY.FileFilter = "All|*.png;*.gif;*.jpg;*.jpeg|PNG|*.png|JPEG|*.jpg;*.jpeg|GIF|*.gif";
            this.ctrlNegY.FileName = "";
            this.ctrlNegY.LabelText = "-Y:";
            this.ctrlNegY.Location = new System.Drawing.Point(0, 60);
            this.ctrlNegY.Name = "ctrlNegY";
            this.ctrlNegY.Size = new System.Drawing.Size(464, 30);
            this.ctrlNegY.TabIndex = 2;
            // 
            // ctrlPosY
            // 
            this.ctrlPosY.DefaultExt = "";
            this.ctrlPosY.Dock = System.Windows.Forms.DockStyle.Top;
            this.ctrlPosY.FileFilter = "All|*.png;*.gif;*.jpg;*.jpeg|PNG|*.png|JPEG|*.jpg;*.jpeg|GIF|*.gif";
            this.ctrlPosY.FileName = "";
            this.ctrlPosY.LabelText = "+Y:";
            this.ctrlPosY.Location = new System.Drawing.Point(0, 90);
            this.ctrlPosY.Name = "ctrlPosY";
            this.ctrlPosY.Size = new System.Drawing.Size(464, 30);
            this.ctrlPosY.TabIndex = 3;
            // 
            // ctrlNegZ
            // 
            this.ctrlNegZ.DefaultExt = "";
            this.ctrlNegZ.Dock = System.Windows.Forms.DockStyle.Top;
            this.ctrlNegZ.FileFilter = "All|*.png;*.gif;*.jpg;*.jpeg|PNG|*.png|JPEG|*.jpg;*.jpeg|GIF|*.gif";
            this.ctrlNegZ.FileName = "";
            this.ctrlNegZ.LabelText = "-Z:";
            this.ctrlNegZ.Location = new System.Drawing.Point(0, 120);
            this.ctrlNegZ.Name = "ctrlNegZ";
            this.ctrlNegZ.Size = new System.Drawing.Size(464, 30);
            this.ctrlNegZ.TabIndex = 4;
            // 
            // ctrlPosZ
            // 
            this.ctrlPosZ.DefaultExt = "";
            this.ctrlPosZ.Dock = System.Windows.Forms.DockStyle.Top;
            this.ctrlPosZ.FileFilter = "All|*.png;*.gif;*.jpg;*.jpeg|PNG|*.png|JPEG|*.jpg;*.jpeg|GIF|*.gif";
            this.ctrlPosZ.FileName = "";
            this.ctrlPosZ.LabelText = "+Z:";
            this.ctrlPosZ.Location = new System.Drawing.Point(0, 150);
            this.ctrlPosZ.Name = "ctrlPosZ";
            this.ctrlPosZ.Size = new System.Drawing.Size(464, 30);
            this.ctrlPosZ.TabIndex = 5;
            // 
            // CubeMapConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ctrlPosZ);
            this.Controls.Add(this.ctrlNegZ);
            this.Controls.Add(this.ctrlPosY);
            this.Controls.Add(this.ctrlNegY);
            this.Controls.Add(this.ctrlPosX);
            this.Controls.Add(this.ctrlNegX);
            this.Name = "CubeMapConfig";
            this.Size = new System.Drawing.Size(464, 180);
            this.ResumeLayout(false);

        }

        #endregion

        private FileConfigItem ctrlNegX;
        private FileConfigItem ctrlPosX;
        private FileConfigItem ctrlNegY;
        private FileConfigItem ctrlPosY;
        private FileConfigItem ctrlNegZ;
        private FileConfigItem ctrlPosZ;
    }
}
