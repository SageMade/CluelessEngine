﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ManifestPacker
{
    public partial class SoundEffectConfig : UserControl
    {
        private SoundEffectItem myItem;

        public SoundEffectItem Item
        {
            get {
                if (myItem != null) {
                    myItem.Source = ctrlSource.FileName;
                    myItem.Pan   = trkPan.Value   / 100.0f;
                    myItem.Pitch = trkPitch.Value / 100.0f;
                    myItem.Gain  = trkGain.Value  / 100.0f;
                }
                return myItem;
            }
            set {
                myItem = value;

                if (value != null)
                {
                    ctrlSource.FileName = myItem.Source;

                    trkPan.Value   = (int)((myItem.Pan)   * 100);
                    trkPitch.Value = (int)((myItem.Pitch) * 100);
                    trkGain.Value  = (int)((myItem.Gain)  * 100);
                }
            }
        }
        public SoundEffectConfig()
        {
            InitializeComponent();            
        }

        private void trkPitch_MouseUp(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Right)
                trkPan.Value = 100;
        }

        private void trkPan_MouseUp(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Right)
                trkPitch.Value = 0;
        }

        private void trkGain_MouseUp(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Right)
                trkGain.Value = 100;
        }
    }
}
