﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace ManifestPacker
{
    class ManifestLoader {

        public static void LoadManifest(string fileName, List<Bundle> bundleResults, TreeView view) {
            XmlDocument document = new XmlDocument();
            document.Load(fileName);

            XmlNodeList manifests = document.GetElementsByTagName("Manifest");

            if (manifests.Count != 1)
                throw new FormatException("Manifest must have 1 manifest node");

            XmlElement package = (XmlElement)manifests[0];

            XmlNodeList bundles = package.GetElementsByTagName("Bundle");

            for(int ix = 0; ix < bundles.Count; ix ++) {
                ReadBundle(bundleResults, view, (XmlElement)bundles[ix]);
            }
        }

        private static void ReadBundle(List<Bundle> bundleResults, TreeView view, XmlElement node) {
            Bundle bundle = new Bundle();
            bundle.Name = node.GetAttribute("name");
            
            TreeNode bundleNode = new TreeNode(bundle.Name);
            bundleNode.Name = bundle.Name;
            bundleNode.Tag = bundle;
            view.Nodes[0].Nodes.Add(bundleNode);

            TreeNode textureNode = new TreeNode("Textures");
            textureNode.Tag = typeof(TextureItem);
            bundleNode.Nodes.Add(textureNode);

            TreeNode shaderNode = new TreeNode("Shaders");
            shaderNode.Tag = typeof(ShaderItem);
            bundleNode.Nodes.Add(shaderNode);

            TreeNode modelNode = new TreeNode("Models");
            modelNode.Tag = typeof(ModelItem);
            bundleNode.Nodes.Add(modelNode);

            TreeNode cubeMapNode = new TreeNode("CubeMaps");
            cubeMapNode.Tag = typeof(CubeMapItem);
            bundleNode.Nodes.Add(cubeMapNode);

            TreeNode soundEffectNode = new TreeNode("Sound Effects");
            soundEffectNode.Tag = typeof(SoundEffectItem);
            bundleNode.Nodes.Add(soundEffectNode);

            XmlNodeList textures = node.GetElementsByTagName("Texture");
            ReadTextures(bundle, view, textures, textureNode);

            XmlNodeList shaders = node.GetElementsByTagName("Shader");
            ReadShaders(bundle, view, shaders, shaderNode);

            XmlNodeList models = node.GetElementsByTagName("Model");
            ReadModels(bundle, view, models, modelNode);

            XmlNodeList cubeMaps = node.GetElementsByTagName("CubeMap");
            ReadCubeMaps(bundle, view, cubeMaps, cubeMapNode);

            XmlNodeList soundEffects = node.GetElementsByTagName("SoundEffect");
            ReadSounds(bundle, view, soundEffects, soundEffectNode);

            view.Nodes[0].ExpandAll();
        }

        private static void ReadTextures(Bundle bundle, TreeView view, XmlNodeList elements, TreeNode parentNode)
        {
            for (int ix = 0; ix < elements.Count; ix++)
            {
                XmlElement element = (XmlElement)elements[ix];

                TextureItem item = new TextureItem(element.GetAttribute("name"));

                item.SourcePath = element.GetAttribute("src");

                XmlNodeList wrapMode = element.GetElementsByTagName("WrapMode");
                if (wrapMode.Count > 0)
                    item.WrapMode = ((XmlElement)wrapMode[0]).GetAttribute("mode");

                XmlNodeList filterMode = element.GetElementsByTagName("FilterMode");
                if (filterMode.Count > 0)
                    item.SampleMode = ((XmlElement)filterMode[0]).GetAttribute("mode");

                XmlNodeList anisoElement = element.GetElementsByTagName("AnisotropyEnabled");
                if (anisoElement.Count > 0)
                    item.AnisoEnabled = true;

                bundle.TextureItems.Add(item);
                
                TreeNode instanceNode = new TreeNode(item.Name);
                instanceNode.Name = item.Name;
                instanceNode.Tag = item;

                instanceNode.Expand();

                parentNode.Nodes.Add(instanceNode);
            }
        }

        private static void ReadShaders(Bundle bundle, TreeView view, XmlNodeList elements, TreeNode parentNode)
        {
            for (int ix = 0; ix < elements.Count; ix++)
            {
                XmlElement element = (XmlElement)elements[ix];

                ShaderItem item = new ShaderItem(element.GetAttribute("name"));

                XmlNodeList vsSource = element.GetElementsByTagName("VsSource");
                if (vsSource.Count > 0)
                    item.VsSourcePath = ((XmlElement)vsSource[0]).GetAttribute("src");

                XmlNodeList fsSource = element.GetElementsByTagName("FsSource");
                if (fsSource.Count > 0)
                    item.FsSourcePath = ((XmlElement)fsSource[0]).GetAttribute("src");

                XmlNodeList gsSource = element.GetElementsByTagName("GsSource");
                if (gsSource.Count > 0)
                    item.GsSourcePath = ((XmlElement)gsSource[0]).GetAttribute("src");

                bundle.ShaderItems.Add(item);

                TreeNode instanceNode = new TreeNode(item.Name);
                instanceNode.Name = item.Name;
                instanceNode.Tag = item;

                instanceNode.Expand();

                parentNode.Nodes.Add(instanceNode);
            }
        }

        private static void ReadModels(Bundle bundle, TreeView view, XmlNodeList elements, TreeNode parentNode)
        {
            for (int ix = 0; ix < elements.Count; ix++)
            {
                XmlElement element = (XmlElement)elements[ix];

                ModelItem item = new ModelItem(element.GetAttribute("name"));

                XmlNodeList sourceFile = element.GetElementsByTagName("SourceFile");
                if (sourceFile.Count > 0)
                {
                    item.SourceFile = ((XmlElement)sourceFile[0]).InnerText;
                    item.Selector = ((XmlElement)sourceFile[0]).GetAttribute("selector");
                }

                bundle.ModelItems.Add(item);

                TreeNode instanceNode = new TreeNode(item.Name);
                instanceNode.Name = item.Name;
                instanceNode.Tag = item;

                instanceNode.Expand();

                parentNode.Nodes.Add(instanceNode);
            }
        }

        private static void ReadCubeMaps(Bundle bundle, TreeView view, XmlNodeList elements, TreeNode parentNode)
        {
            for (int ix = 0; ix < elements.Count; ix++)
            {
                XmlElement element = (XmlElement)elements[ix];

                CubeMapItem item = new CubeMapItem(element.GetAttribute("name"));

                XmlNodeList negX = element.GetElementsByTagName("NegX");
                if (negX.Count > 0) { 
                    item.NegXSource = ((XmlElement)negX[0]).GetAttribute("src");
                }
                XmlNodeList posX = element.GetElementsByTagName("PosX");
                if (negX.Count > 0) {
                    item.PosXSource = ((XmlElement)posX[0]).GetAttribute("src");
                }

                XmlNodeList negY = element.GetElementsByTagName("NegY");
                if (negX.Count > 0) {
                    item.NegYSource = ((XmlElement)negY[0]).GetAttribute("src");
                }
                XmlNodeList posY = element.GetElementsByTagName("PosY");
                if (negX.Count > 0) {
                    item.PosYSource = ((XmlElement)posY[0]).GetAttribute("src");
                }

                XmlNodeList negZ = element.GetElementsByTagName("NegZ");
                if (negX.Count > 0) {
                    item.NegZSource = ((XmlElement)negZ[0]).GetAttribute("src");
                }
                XmlNodeList posZ = element.GetElementsByTagName("PosZ");
                if (negX.Count > 0) {
                    item.PosZSource = ((XmlElement)posZ[0]).GetAttribute("src");
                }

                bundle.CubeMapItems.Add(item);

                TreeNode instanceNode = new TreeNode(item.Name);
                instanceNode.Name = item.Name;
                instanceNode.Tag = item;

                instanceNode.Expand();

                parentNode.Nodes.Add(instanceNode);
            }
        }

        private static void ReadSounds(Bundle bundle, TreeView view, XmlNodeList elements, TreeNode parentNode)
        {
            for (int ix = 0; ix < elements.Count; ix++)
            {
                XmlElement element = (XmlElement)elements[ix];

                SoundEffectItem item = new SoundEffectItem(element.GetAttribute("name"));

                item.Source = element.GetAttribute("src");

                if (!float.TryParse(element.GetAttribute("pan"), out item.Pan))
                    item.Pan = 0.0f;
                if (!float.TryParse(element.GetAttribute("pitch"), out item.Pitch))
                    item.Pitch = 1.0f;
                if (!float.TryParse(element.GetAttribute("gain"), out item.Gain))
                    item.Gain = 1.0f;
                
                bundle.SoundItems.Add(item);

                TreeNode instanceNode = new TreeNode(item.Name);
                instanceNode.Name = item.Name;
                instanceNode.Tag = item;

                instanceNode.Expand();

                parentNode.Nodes.Add(instanceNode);
            }
        }

        public static void SaveManifest(string fileName, TreeNode rootNode)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.IndentChars = "\t";
            settings.NewLineChars = Environment.NewLine;
            settings.Indent = true;

            XmlWriter writer = XmlWriter.Create(fileName, settings);

            writer.WriteStartDocument();

            writer.WriteStartElement("Manifest");

            for(int ix = 0; ix < rootNode.Nodes.Count; ix++) {
                WriteBundle(writer, (Bundle)rootNode.Nodes[ix].Tag);
            }

            writer.WriteEndElement();

            writer.WriteEndDocument();

            writer.Close();
        }

        private static void WriteBundle(XmlWriter writer, Bundle bundle) {
            writer.WriteStartElement("Bundle");

            writer.WriteAttributeString("name", bundle.Name);

            for(int ix =0; ix < bundle.TextureItems.Count; ix++) {
                WriteTexture(writer, bundle.TextureItems[ix]);
            }

            for (int ix = 0; ix < bundle.SoundItems.Count; ix++) {
                WriteSound(writer, bundle.SoundItems[ix]);
            }

            for (int ix = 0; ix < bundle.ShaderItems.Count; ix++) {
                WriteShader(writer, bundle.ShaderItems[ix]);
            }

            for (int ix = 0; ix < bundle.CubeMapItems.Count; ix++) {
                WriteCubeMap(writer, bundle.CubeMapItems[ix]);
            }

            for (int ix = 0; ix < bundle.ModelItems.Count; ix++)
            {
                WriteModel(writer, bundle.ModelItems[ix]);
            }

            writer.WriteEndElement();
        }
        
        private static void WriteTexture(XmlWriter writer, TextureItem item) {
            writer.WriteStartElement("Texture");

            writer.WriteAttributeString("name", item.Name);
            writer.WriteAttributeString("src", item.SourcePath);

            if (item.WrapMode != null && item.WrapMode != "edge") {
                writer.WriteStartElement("WrapMode");
                writer.WriteAttributeString("mode", item.WrapMode);
                writer.WriteEndElement();
            }

            if (item.SampleMode != null && item.SampleMode != "linear")
            {
                writer.WriteStartElement("FilterMode");
                writer.WriteAttributeString("mode", item.SampleMode);
                writer.WriteEndElement();
            }

            if (item.AnisoEnabled)
            {
                writer.WriteStartElement("AnisotropyEnabled");
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        private static void WriteSound(XmlWriter writer, SoundEffectItem item)
        {
            writer.WriteStartElement("SoundEffect");

            writer.WriteAttributeString("name", item.Name);
            writer.WriteAttributeString("src", item.Source);

            if (item.Gain != 1.0f)
                writer.WriteAttributeString("gain", item.Gain.ToString());

            if (item.Pan != 0.0f)
                writer.WriteAttributeString("pan", item.Pan.ToString());

            if (item.Pitch != 1.0f)
                writer.WriteAttributeString("pitch", item.Pitch.ToString());
            
            writer.WriteEndElement();
        }

        private static void WriteShader(XmlWriter writer, ShaderItem item)
        {
            writer.WriteStartElement("Shader");

            writer.WriteAttributeString("name", item.Name);

            writer.WriteStartElement("VsSource");
            writer.WriteAttributeString("src", item.VsSourcePath);
            writer.WriteEndElement();

            writer.WriteStartElement("FsSource");
            writer.WriteAttributeString("src", item.FsSourcePath);
            writer.WriteEndElement();

            writer.WriteStartElement("GsSource");
            writer.WriteAttributeString("src", item.GsSourcePath);
            writer.WriteEndElement();
            
            writer.WriteEndElement();
        }

        private static void WriteCubeMap(XmlWriter writer, CubeMapItem item)
        {
            writer.WriteStartElement("CubeMap");

            writer.WriteAttributeString("name", item.Name);

            writer.WriteStartElement("NegX");
            writer.WriteAttributeString("src", item.NegXSource);
            writer.WriteEndElement();
            writer.WriteStartElement("PosX");
            writer.WriteAttributeString("src", item.PosXSource);
            writer.WriteEndElement();

            writer.WriteStartElement("NegY");
            writer.WriteAttributeString("src", item.NegYSource);
            writer.WriteEndElement();
            writer.WriteStartElement("PosY");
            writer.WriteAttributeString("src", item.PosYSource);
            writer.WriteEndElement();

            writer.WriteStartElement("NegZ");
            writer.WriteAttributeString("src", item.NegZSource);
            writer.WriteEndElement();
            writer.WriteStartElement("PosZ");
            writer.WriteAttributeString("src", item.PosZSource);
            writer.WriteEndElement();

            writer.WriteEndElement();
        }

        private static void WriteModel(XmlWriter writer, ModelItem item)
        {
            writer.WriteStartElement("Model");

            writer.WriteAttributeString("name", item.Name);

            writer.WriteStartElement("SourceFile");
            if (item.Selector != null && item.Selector != "root")
                writer.WriteAttributeString("selector", item.Selector);
            writer.WriteString(item.SourceFile);
            writer.WriteEndElement();

            writer.WriteEndElement();
        }
    }
}
