﻿namespace ManifestPacker
{
    public class ModelItem
    {
        public string Name;

        public string SourceFile;
        public string Selector;

        public ModelItem(string name)
        {
            Name = name;
        }
    }
}