﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ManifestPacker
{
    public partial class ShaderConfig : UserControl
    {
        private ShaderItem myItem;

        public ShaderItem Item
        {
            get
            {
                if (myItem != null)
                {
                    myItem.FsSourcePath  = ctrlFsSource.FileName;
                    myItem.VsSourcePath  = ctrlVsSource.FileName;
                    myItem.GsSourcePath  = ctrlGsSource.FileName;
                }

                return myItem;
            }
            set
            {
                myItem = value;

                if (myItem != null)
                {
                    ctrlFsSource.FileName = myItem.FsSourcePath;
                    ctrlVsSource.FileName = myItem.VsSourcePath;
                    ctrlGsSource.FileName = myItem.GsSourcePath;
                }
            }
        }
        public ShaderConfig()
        {
            InitializeComponent();
        }
    }
}
