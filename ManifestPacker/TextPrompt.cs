﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ManifestPacker
{
    public partial class TextPrompt : Form
    {
        public string ResultText
        {
            get { return txtInput.Text; }
            set { txtInput.Text = value; }
        }
        public string Prompt
        {
            get { return Text; }
            set { Text = value; }
        }

        public TextPrompt(string prompt, string defaultText ="")
        {
            InitializeComponent();

            Text = prompt;
            txtInput.Text = defaultText;
        }
    }
}
