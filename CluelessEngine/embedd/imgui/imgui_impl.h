#pragma once
#include "imgui/imgui.h"

namespace clueless { namespace imgui_impl {

	void ImGuiInit();
	void ImGuiDraw(ImDrawData* draw_data);

} }