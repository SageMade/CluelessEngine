#include "graphics/MeshData.h"

#include "graphics/VertexLayouts.h"

namespace clueless { namespace graphics {

		MeshData MeshData::LoadFromScene(const aiScene * scene, int meshIndex)
		{
			aiMesh* mesh = scene->mMeshes[meshIndex];

			FullVertex* vertices = new FullVertex[mesh->mNumVertices];
			USHORT*     indices = new USHORT[mesh->mNumFaces * 3];

			// Walk through each of the mesh's vertices
			for (unsigned int i = 0; i < mesh->mNumVertices; i++)
			{
				FullVertex vertex;
				glm::vec3 vector; // we declare a placeholder vector since assimp uses its own vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
								  // positions
				vector.x = mesh->mVertices[i].x;
				vector.y = mesh->mVertices[i].y;
				vector.z = mesh->mVertices[i].z;
				vertex.Position = vector;
				// normals
				vector.x = mesh->mNormals[i].x;
				vector.y = mesh->mNormals[i].y;
				vector.z = mesh->mNormals[i].z;
				vertex.Normal = vector;
				// texture coordinates
				if (mesh->mTextureCoords[0]) // does the mesh contain texture coordinates?
				{
					glm::vec2 vec;
					// a vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't 
					// use models where a vertex can have multiple texture coordinates so we always take the first set (0).
					vec.x = mesh->mTextureCoords[0][i].x;
					vec.y = mesh->mTextureCoords[0][i].y;
					vertex.Texture = vec;
				}
				else
					vertex.Texture = glm::vec2(0.0f, 0.0f);
				//// tangent
				if (mesh->mTangents) {
					vector.x = mesh->mTangents[i].x;
					vector.y = mesh->mTangents[i].y;
					vector.z = mesh->mTangents[i].z;
					vertex.Tangent = vector;
				}
				else {
					vertex.Tangent = glm::vec3(0.0f);
				}
				//// bitangent
				if (mesh->mTangents) {
					vector.x = mesh->mBitangents[i].x;
					vector.y = mesh->mBitangents[i].y;
					vector.z = mesh->mBitangents[i].z;
					vertex.BiTangent = vector;
				}
				else {
					vertex.BiTangent = glm::vec3(0.0f);
				}

				vertices[i] = vertex;
			}
			// now walk through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
			for (GLuint i = 0; i < mesh->mNumFaces; i++)
			{
				aiFace face = mesh->mFaces[i];
				// retrieve all indices of the face and store them in the indices vector
				for (GLuint j = 0; j < face.mNumIndices; j++)
					indices[i * 3 + j] = face.mIndices[j];
			}

			USHORT vCount, iCount;
			vCount = mesh->mNumVertices;
			iCount = mesh->mNumFaces * 3;
			return MeshData(vertices, vCount, indices, iCount, &FullVertexDecl);
		}

} }
