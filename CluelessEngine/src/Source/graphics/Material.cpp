#include "graphics/Material.h"

#include "cms/CMS.h"

namespace clueless { namespace graphics {


	int Material::MaterialCount = 0;
	int Material::BoundMaterial = -1;

    Material::Material() {
		myNormalTex = myDiffuseTex = mySpecularTex = myEmissiveTex = nullptr;
		myMaterialId = MaterialCount;
		MaterialCount++;
    }

    Material::~Material()
    {
        //dtor
    }

	Material* Material::FromAssimp(aiMaterial & material) {
		Material *result = new Material();
		aiString str;
		
		result->myDiffuseTex = __TryGetTexture(material, aiTextureType_DIFFUSE);
		result->myNormalTex = __TryGetTexture(material, aiTextureType_NORMALS);
		result->mySpecularTex = __TryGetTexture(material, aiTextureType_SPECULAR);
		result->myEmissiveTex = __TryGetTexture(material, aiTextureType_EMISSIVE);
		result->myDiffuseTex = __TryGetTexture(material, aiTextureType_DIFFUSE);

		material.Get(AI_MATKEY_SHININESS, result->myShininess);
		material.Get(AI_MATKEY_SHADING_MODEL, result->mySuggestedShading);

		aiColor3D temp;
		
		material.Get(AI_MATKEY_COLOR_DIFFUSE, temp);
		result->myDiffuseColor = glm::vec3(temp.r, temp.g, temp.b);

		material.Get(AI_MATKEY_COLOR_AMBIENT, temp);
		result->myAmbientColor = glm::vec3(temp.r, temp.g, temp.b);

		material.Get(AI_MATKEY_COLOR_SPECULAR, temp);
		result->mySpecularColor = glm::vec3(temp.r, temp.g, temp.b);

		material.Get(AI_MATKEY_COLOR_EMISSIVE, temp);
		result->myEmissiveColor = glm::vec3(temp.r, temp.g, temp.b);
		
		return result;
	}

	Texture* Material::__TryGetTexture(aiMaterial& material, aiTextureType type) {
		aiString str;
		if (material.GetTextureCount(type) > 0) {
			material.GetTexture(type, 0, &str);
			//Texture t = CMS::Singleton.Get<Texture>(str.C_Str(), "texture");
			return nullptr;
		}
		else {
			return nullptr;
		}
	}

	void Material::SetTexture(Texture * texture, TextureUsage usage) {
		switch (usage) {
			case TextureDiffuse:
				myDiffuseTex = texture;
				break;
			case TextureSpecular:
				mySpecularTex = texture;
				break;
			case TextureEmissive:
				myEmissiveTex = texture;
				break;
			case TextureNormal:
				myNormalTex = texture;
				break;
		}
	}

	void Material::Unload() {
		// Should be handled by CMS
	}

	void Material::Apply() {
		Shader* shader = CMS::Redeem<Shader>(myShader);
		shader->Enable();

		if (myMaterialId != BoundMaterial) {
			if (myDiffuseTex != nullptr)
				myDiffuseTex->Bind(0U);
			else
				Texture::Unbind(0U);
			if (myNormalTex != nullptr)
				myNormalTex->Bind(1U);
			else
				Texture::Unbind(1U);
			if (mySpecularTex != nullptr)
				mySpecularTex->Bind(2U);
			else
				Texture::Unbind(2U);
			if (myEmissiveTex != nullptr)
				myEmissiveTex->Bind(3U);
			else
				Texture::Unbind(3U);

			shader->SetUniform("xDiffuse", 0);
			shader->SetUniform("xNormal", 1);
			shader->SetUniform("xSpecular", 2);
			shader->SetUniform("xEmissive", 3);

			OnApply();
		}
	}

	void Material::SetWorldTransform(const glm::mat4& transform) {
		CMS::Redeem<Shader>(myShader)->SetUniform("xWorld", transform);
	}

	void Material::SetModelTransform(const glm::mat4& transform) {
		CMS::Redeem<Shader>(myShader)->SetUniform("xModel", transform);
	}
	void Material::SetViewMatrix(const glm::mat4 & value) {
		CMS::Redeem<Shader>(myShader)->SetUniform("xView", value);
	}
	void Material::SetProjectionMatrix(const glm::mat4 & value) {
		CMS::Redeem<Shader>(myShader)->SetUniform("xProjection", value);
	}
	void Material::LoadIdentity() {
		Shader* shader = CMS::Redeem<Shader>(myShader);

		if (shader) {
			glm::mat4 identity = glm::mat4();
			shader->SetUniform("xWorld", identity);
			shader->SetUniform("xModel", identity);
			shader->SetUniform("xView", identity);
			shader->SetUniform("xProjection", identity);
		}
	}
}}
