#include "graphics/SkinnedMesh.h"

#include "graphics/Material.h"
#include "game_logic/Scene.h"
#include "graphics/VertexLayouts.h"

#define BINDIDX_SKINNED_MESH_DATA 4

namespace clueless { namespace graphics {

	SkinnedMesh::SkinnedMesh(uint16_t numBones) {
	}


	SkinnedMesh::~SkinnedMesh() {
	}
	
	void SkinnedMesh::Unload() {
		myMesh->Unload();
		delete myMesh;
		myMesh = nullptr;

		myBuffer->Unload();
		delete myBuffer;
		myBuffer = nullptr;

		myMaterialToken = ContentToken();
	}

	void SkinnedMesh::LoadFromScene(const aiScene *scene, int meshIndex, const ContentToken& materialToken) {

		myMaterialToken = materialToken;
		myBuffer = new UniformBuffer();

		Material *material = CMS::Redeem<Material>(myMaterialToken);
		if (material) {
			ContentToken shaderToken = material->GetShaderToken();
			Shader *shader = CMS::Redeem<Shader>(shaderToken);

			if (shader) {
				int numBones = shader->GetBlockElementCount<glm::mat4>("xBones");
				shader->SetBufferBindingPoint("xBones", BINDIDX_SKINNED_MESH_DATA);
				myBuffer->Init();
				myBuffer->Bind(BINDIDX_SKINNED_MESH_DATA);
				myBuffer->Alloc<glm::mat4>(numBones);

				for (int ix = 0; ix < numBones; ix++) {
					myBuffer->GetData<glm::mat4>()[ix] = glm::mat4();
				}
			}
		}

		aiMesh *mesh = scene->mMeshes[meshIndex];

		SkinnedVert* vertices = new SkinnedVert[mesh->mNumVertices];
		uint16_t*    indices  = new uint16_t[mesh->mNumFaces * 3];

		// Walk through each of the mesh's vertices
		for (unsigned int i = 0; i < mesh->mNumVertices; i++)
		{
			SkinnedVert vertex;
			glm::vec3 vector; // we declare a placeholder vector since assimp uses its own vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
							  // positions
			vector.x = mesh->mVertices[i].x;
			vector.y = mesh->mVertices[i].y;
			vector.z = mesh->mVertices[i].z;
			vertex.Position = vector;
			// normals
			vector.x = mesh->mNormals[i].x;
			vector.y = mesh->mNormals[i].y;
			vector.z = mesh->mNormals[i].z;
			vertex.Normal = vector;
			// texture coordinates
			if (mesh->mTextureCoords[0]) // does the mesh contain texture coordinates?
			{
				glm::vec2 vec;
				// a vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't 
				// use models where a vertex can have multiple texture coordinates so we always take the first set (0).
				vec.x = mesh->mTextureCoords[0][i].x;
				vec.y = mesh->mTextureCoords[0][i].y;
				vertex.Texture = vec;
			}
			else
				vertex.Texture = glm::vec2(0.0f, 0.0f);
			//// tangent
			if (mesh->mTangents) {
				vector.x = mesh->mTangents[i].x;
				vector.y = mesh->mTangents[i].y;
				vector.z = mesh->mTangents[i].z;
				vertex.Tangent = vector;
			}
			else {
				vertex.Tangent = glm::vec3(0.0f);
			}
			//// bitangent
			if (mesh->mTangents) {
				vector.x = mesh->mBitangents[i].x;
				vector.y = mesh->mBitangents[i].y;
				vector.z = mesh->mBitangents[i].z;
				vertex.BiTangent = vector;
			}
			else {
				vertex.BiTangent = glm::vec3(0.0f);
			}

			vertex.BoneIDs     = glm::ivec4(-1);
			vertex.BoneWeights = glm::vec4(0);

			vertices[i] = vertex;
		}
		// now walk through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
		for (GLuint i = 0; i < mesh->mNumFaces; i++)
		{
			aiFace face = mesh->mFaces[i];
			// retrieve all indices of the face and store them in the indices vector
			for (GLuint j = 0; j < face.mNumIndices; j++)
				indices[i * 3 + j] = face.mIndices[j];
		}
		
		myBoneCount = mesh->mNumBones;
		
		if (myBoneCount > 0) {
			for (int ix = 0; ix < myBoneCount; ix++) {
				aiBone *bone = mesh->mBones[ix];
				
				for (int jx = 0; jx < bone->mNumWeights; jx++) {
					aiVertexWeight weight = bone->mWeights[jx];

					for (int kx = 0; kx < 4; kx++) {
						if (vertices[weight.mVertexId].BoneIDs[kx] == -1) {
							vertices[weight.mVertexId].BoneIDs[kx] = ix;
							vertices[weight.mVertexId].BoneWeights[kx] = weight.mWeight;
							break;
						}
					}
				}
			}
		}

		uint16_t vCount, iCount;
		vCount = mesh->mNumVertices;
		iCount = mesh->mNumFaces * 3;
		MeshData meshDat = MeshData(vertices, vCount, indices, iCount, &SkinnedVertDecl);
		myMesh = new Mesh(meshDat);
	}
	
	void SkinnedMesh::Draw(const glm::mat4& world, const glm::mat4& view, const glm::mat4& projection) const {
		Material *material = CMS::Redeem<Material>(myMaterialToken);

		if (material != nullptr) {
			material->Apply();
			material->SetWorldTransform(world);
			material->SetProjectionMatrix(projection);
			material->SetViewMatrix(view);
		}

		myBuffer->Bind(BINDIDX_SKINNED_MESH_DATA);
		myBuffer->Update();

		myMesh->Draw();
	}

} }