#include "game_logic/Scene.h"

#include <chrono>

#include <iostream>

#include "ui/input/InputManager.h"

#include "graphics/PrimitiveBatch3D.h"
#include "graphics/PrimitiveBatch2D.h"

#include "ui/ImGuiTools.h"

using namespace std::chrono;

namespace clueless { namespace game_logic {


	std::vector<GameObject*> Scene::myGameObjects;
	TypeMap<GameSystem>      Scene::mySystems;

	ICamera		          *Scene::myActiveCamera;

	physics::PhysicsWorld  *Scene::myPhysicsWorld;
	graphics::Window       *Scene::myWindow;

	float Scene::myFrameDelta;
	float Scene::myFixedFrameTime;

	void Scene::Init() {
		myFixedFrameTime = 1.0f / 60.0f;
		myFrameDelta = 0.0f;

		myPhysicsWorld = new physics::PhysicsWorld();
		myPhysicsWorld->SetGravity(0, 0, -9.8f);

		graphics::PrimitiveBatch2D::Init();
		graphics::PrimitiveBatch3D::Init();
		ImGuiTools::Init();
	}

	void Scene::Load() {

	}

	void Scene::Start() {
		for (int ix = 0; ix < myGameObjects.size(); ix++) {
			myGameObjects[ix]->OnStart();
		}
	}

	void Scene::Update() {		
		auto startTime = high_resolution_clock::now();
		static uint32_t frameMicros = 0;

		myFrameDelta += GameTime::DeltaTime;

		for (int ix = 0; ix < mySystems.Count(); ix++) {
			for (int ox = 0; ox < myGameObjects.size(); ox++) {
				mySystems.GetAt(ix)->Update(myGameObjects[ox]);
			}
		}

		for (int ox = 0; ox < myGameObjects.size(); ox++) {
			myGameObjects[ox]->Update();
		}

		for (int ix = 0; ix < mySystems.Count(); ix++) {
			for (int ox = 0; ox < myGameObjects.size(); ox++) {
				mySystems.GetAt(ix)->LateUpdate(myGameObjects[ox]);
			}
		}

		for (int ox = 0; ox < myGameObjects.size(); ox++) {
			myGameObjects[ox]->LateUpdate();
		}

		if (myFrameDelta > myFixedFrameTime) {
			GameTime::FrameDeltaTime = myFrameDelta;
			myFrameDelta -= myFixedFrameTime;

			if (myPhysicsWorld) {

				myPhysicsWorld->Update();
			
				for (int ix = 0; ix < mySystems.Count(); ix++) {
					mySystems.GetAt(ix)->PrePhysics();
				}

				for (int ox = 0; ox < myGameObjects.size(); ox++) {
					myGameObjects[ox]->PrePhysics();
				}
			}

			for (int ix = 0; ix < mySystems.Count(); ix++) {
				for (int ox = 0; ox < myGameObjects.size(); ox++) {
					mySystems.GetAt(ix)->FixedUpdate(myGameObjects[ox]);
				}
			}

			for (int ox = 0; ox < myGameObjects.size(); ox++) {
				myGameObjects[ox]->FixedUpdate();
			}

			ImGuiTools::NewFrame();

			myActiveCamera->PreRender();

			graphics::PrimitiveBatch3D::ViewProj = myActiveCamera->GetProjection() * myActiveCamera->GetView();
			graphics::PrimitiveBatch2D::ViewProj = glm::orthoLH(0.0f, (float)myWindow->GetWidth(), (float)myWindow->GetHeight(), 0.0f, -1.0f, 1.0f);
			
			for (int ix = 0; ix < mySystems.Count(); ix++) {
				mySystems.GetAt(ix)->PreRender();

				for (int ox = 0; ox < myGameObjects.size(); ox++) {
					mySystems.GetAt(ix)->Render(myGameObjects[ox]);
				}

				mySystems.GetAt(ix)->PostRender();
			}

			graphics::PrimitiveBatch3D::Flush();
			graphics::PrimitiveBatch2D::Flush();

			myActiveCamera->PostRender();

			ImGuiTools::RenderFrame();

			myWindow->SwapBuffers();

			ui::input::InputManager::NotifyFrame();
		}

		auto endTime = high_resolution_clock::now();
		auto diff = endTime - startTime;
		frameMicros = (duration_cast<microseconds>(diff)).count();
		startTime = endTime;
	}
	void Scene::Unload() {

	}

	void Scene::Cleanup() {
		while (myGameObjects.size() > 0) {
			delete myGameObjects.back();
			myGameObjects.pop_back();
		}
	}
	
	void Scene::AddObject(GameObject *object) {
		myGameObjects.push_back(object);
		object->AddedToScene();
	}

	void Scene::RemoveObject(GameObject * object) {
		for (int ix = 0; ix < myGameObjects.size(); ix++) {
			if (myGameObjects[ix] == object) {
				myGameObjects.erase(myGameObjects.begin() + ix);
				break;
			}
		}
	}

	void Scene::AddCollisionItem(physics::RigidBody *body) {
		myPhysicsWorld->AddRigidBody(body);
	}

	void Scene::AddCollisionItem(btCollisionObject *body) {
		myPhysicsWorld->GetInternalWorld()->addCollisionObject(body);
	}

	template <typename T>
	void Scene::DeleteList(std::vector<T*> list) {
		for (int ix = 0; ix < list.size(); ix++) {
			delete list[ix];
		}
		list.clear();
	}

}}
