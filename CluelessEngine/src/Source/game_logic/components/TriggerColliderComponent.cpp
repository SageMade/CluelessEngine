#include "game_logic/components/TriggerColliderComponent.h"

#include "game_logic/Scene.h"

#include "physics/EventStructs.h"

namespace clueless { namespace game_logic { namespace components {
	TriggerColliderComponent::TriggerColliderComponent(GameObject * parent, btCollisionShape * shape) :
		GameComponent(parent),
		myCallback(nullptr),
		myIsTriggered(false),
		myTriggerOnEmpty(false),
		myIsEmpty(true)
	{
		myGhost = new btPairCachingGhostObject();
		myGhost->setCollisionShape(shape);

		btTransform transform;

		static btTransform  tempTransform = btTransform();
		static glm::vec3 scale, translate, skew;
		static glm::vec4 perspective;
		static glm::quat orientation;
		glm::decompose(parent->GetWorldTransform(), scale, orientation, translate, skew, perspective);

		tempTransform.setOrigin(glm2bt(translate));
		tempTransform.setRotation(glm2bt(orientation));

		myGhost->setWorldTransform(tempTransform);

		FilterMask = 0xFFFF;
	}

	void TriggerColliderComponent::AddedToScene() {
		myGhost->setCollisionFlags(myGhost->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);
		Scene::AddCollisionItem(myGhost);

	}

	void TriggerColliderComponent::FixedUpdate() {
		btManifoldArray manifoldArray;
		btBroadphasePairArray& pairArray = myGhost->getOverlappingPairCache()->getOverlappingPairArray();
		int numPairs = pairArray.size();

		int numDynamicPairs = 0;

		for (int i = 0; i < numPairs; ++i)
		{
			manifoldArray.clear();

			const btBroadphasePair& pair = pairArray[i];

			btBroadphasePair* collisionPair = 
				Scene::GetPhysicsWorld()->GetInternalWorld()->getPairCache()->findPair(
					pair.m_pProxy0, pair.m_pProxy1);

			if (!collisionPair) continue;

			if (collisionPair->m_algorithm)
				collisionPair->m_algorithm->getAllContactManifolds(manifoldArray);

			for (int j = 0; j < manifoldArray.size(); j++)
			{
				btPersistentManifold* manifold = manifoldArray[j];

				bool isFirstBody = manifold->getBody0() == myGhost;

				const btCollisionObject *obj = (isFirstBody ? manifold->getBody1() : manifold->getBody0());
				
				if (obj->getBroadphaseHandle()->m_collisionFilterGroup & FilterMask) {

					if (obj->getInternalType() == btCollisionObject::CO_RIGID_BODY) {
						const btRigidBody *body = (const btRigidBody *)obj;


						if (body->getInvMass() != 0) {
							numDynamicPairs++;

							if (!myIsTriggered) {
								MessageEvent message(MESSSAGE_VOLUME_TRIGGER);
								message.SetMeta(obj);
								Dispatch(message);
								myIsTriggered = true;
								
								if (!myTriggerOnEmpty) {
									if (myCallback)
										myCallback(myObject);
								}
							}
						}
					}

				}
			}
		}

		if (numDynamicPairs == 0)
		{
			myIsTriggered = false;
			if (!myIsEmpty) {
				myIsEmpty = true;
				if (myTriggerOnEmpty)
				{
					if (myCallback)
						myCallback(myObject);
				}
			}
		}
		else {
			myIsEmpty = false;
		}
	}

} } }