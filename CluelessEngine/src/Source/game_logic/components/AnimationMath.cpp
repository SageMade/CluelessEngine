#ifndef ANIM_MATH
#define ANIM_MATH

#include "../../../Headers/game_logic/components/AnimationMath.h"


namespace glm {
	float distance(const Transform& left, const Transform& right) {
		return glm::distance(left.Position, right.Position) +
			glm::distance((float)left.Orientation.length(), (float)right.Orientation.length()) +
			glm::distance(left.Scale, right.Scale);
	}
}

template <>
Transform AnimationMath::lerp<Transform>(Transform left, Transform right, float t) {
	Transform result;
	result.Position = lerp(left.Position, right.Position, t);
	result.Orientation = glm::slerp(left.Orientation, right.Orientation, t);
	result.Scale = lerp(left.Scale, right.Scale, t);
	return result;
}

#endif