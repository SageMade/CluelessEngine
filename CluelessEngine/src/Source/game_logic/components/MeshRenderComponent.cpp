#include "game_logic/components/MeshRenderComponent.h"

#include "game_logic/Scene.h";

namespace clueless { namespace game_logic{
	using namespace graphics;

	MeshRenderComponent::MeshRenderComponent(GameObject * parent, Model * mesh, Material* material) :
		GameComponent(parent), myModel(mesh), myMaterial(material) {
	}

	MeshRenderComponent::~MeshRenderComponent() { }

	void MeshRenderComponent::Render() {

		if (myMaterial != nullptr) {
			myMaterial->Apply();
			myMaterial->SetModelTransform(glm::mat4());
			myMaterial->SetWorldTransform(myObject->GetWorldTransform());
			myMaterial->SetProjectionMatrix(Scene::GetActiveCamera()->GetProjection());
			myMaterial->SetViewMatrix(Scene::GetActiveCamera()->GetView());
		}

		myModel->Draw();
	}
} }
