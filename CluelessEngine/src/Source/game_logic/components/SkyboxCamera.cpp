#include "game_logic/components/SkyboxCamera.h"

#include "GraphicsConfig.h"

namespace clueless { namespace game_logic { namespace components {

	SkyboxCamera::SkyboxCamera(GameObject *parent) : ICamera(parent)  {
		Skybox skybox = Skybox(parent);
		skybox.Load();
		myObject->AddComponent(skybox);

		myProjection = glm::perspectiveFov(glm::radians(60.0f), (float)GraphicsConfig::ScreenWidth, (float)GraphicsConfig::ScreenHeight, 0.1f, 1000.0f);
		myCameraSpaceToWorld = glm::rotate(M_PI_2F, glm::vec3(1.0f, 0.0f, 0.0f));
	}

	SkyboxCamera::~SkyboxCamera()
	{
	}

	void SkyboxCamera::PreRender() {
		myView = glm::inverse((glm::mat4)myObject->LocalTransform);

		glDisable(GL_DEPTH_TEST);
		Render();
		glEnable(GL_DEPTH_TEST);

	};

} } }
