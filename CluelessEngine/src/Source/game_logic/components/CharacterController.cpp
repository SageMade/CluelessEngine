#include "game_logic/components/CharacterController.h"

#include <BulletCollision\CollisionShapes\btCapsuleShape.h>
#include <BulletDynamics\Dynamics\btRigidBody.h>
#include <BulletDynamics\ConstraintSolver\btGeneric6DofConstraint.h>

#include "game_logic/Scene.h"

#include "game_logic/components/CameraController.h"

#include "ui/input/InputManager.h"

#include "graphics/PrimitiveBatch3D.h"

namespace clueless { namespace game_logic { namespace components {

	using namespace ui::input;

	bool CharacterController:: disabled=false;

	CharacterController::CharacterController(GameObject * parent)
		: GameComponent(parent) 
	{
		float mass = 1.0f;

		myGhost = new btPairCachingGhostObject();
		myShape = new btCapsuleShapeZ(0.5f, 0.75f);

		btVector3 inertia;
		myShape->calculateLocalInertia(mass, inertia);
		myMotion = new btDefaultMotionState(btTransform(btQuaternion(0.0f, 0.0f, 0.0f), btVector3(18.0f, 0.0f, 0.5f)));
		myBody = new btRigidBody(mass, myMotion, myShape, inertia);
		myBody->forceActivationState(DISABLE_DEACTIVATION);
		myBody->setAngularFactor(btVector3(0.0f, 0.0f, 1.0f));
	}


	CharacterController::~CharacterController() {

	}

	void CharacterController::AddedToScene() {
		Scene::GetPhysicsWorld()->GetInternalWorld()->addRigidBody(myBody, COL_PLAYER, 0xFFFF);
	}
	
	void CharacterController::PrePhysics() {
		btTransform &trans = myBody->getWorldTransform();
		trans.setRotation(btQuaternion(0.0f, 0.0f, myYaw));
		glm::vec3 pos = bt2glm(myMotion->m_graphicsWorldTrans.getOrigin()) + glm::vec3(0.0f, 0.0f, 0.8f);
		myObject->LocalTransform.Position = pos;
	}

	void CharacterController::FixedUpdate() {
		myYaw = glm::radians(Scene::GetActiveCamera()->GetParent()->GetComponent<CameraController>()->GetYaw()+glm::radians(90.0f));
		

		float speed = 4.0f;
		glm::vec3 transform = glm::vec3(0.0f, 0.0f, 1.0f);




			if (InputManager::GetKeyState(GLFW_KEY_W) != ButtonReleased) {
				transform.x -= 1;
			}
			if (InputManager::GetKeyState(GLFW_KEY_S) != ButtonReleased) {
				transform.x += 1;
			}
			if (InputManager::GetKeyState(GLFW_KEY_D) != ButtonReleased) {
				transform.y += 1;
			}
			if (InputManager::GetKeyState(GLFW_KEY_A) != ButtonReleased) {
				transform.y -= 1;
			}
		
			if (disabled == true)
			{
				transform = glm::vec3(0.0f, 0.0f, 1.0f);
			}

		transform = glm::normalize(transform * glm::quat(glm::vec3(0.0f, 0.0f, -myYaw)));
		transform.z = 0;
		transform *= speed;

		if (InputManager::GetKeyState(GLFW_KEY_SPACE) != ButtonReleased) {
			btDynamicsWorld *world = Scene::GetPhysicsWorld()->GetInternalWorld();

			glm::vec3 from = myObject->LocalTransform.Position;
			glm::vec3 to = myObject->LocalTransform.Position - glm::vec3(0.0f, 0.0f, 1.75f);
			btCollisionWorld::ClosestRayResultCallback RayCallback(glm2bt(from), glm2bt(to));
			//RayCallback.m_collisionFilterMask = ~COL_PLAYER;
			world->rayTest(RayCallback.m_rayFromWorld, RayCallback.m_rayToWorld, RayCallback);

			if (RayCallback.hasHit()) {
				myBody->applyCentralImpulse(btVector3(0.0f, 0.0f, 1.0f));
			}
		}
		transform.z = myBody->getLinearVelocity().z();
		myBody->activate(true);
		myBody->setLinearVelocity(glm2bt(transform));
	}

} } }