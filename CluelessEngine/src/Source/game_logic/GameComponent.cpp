#include "game_logic/GameComponent.h"

namespace clueless { namespace game_logic {
	GameComponent::GameComponent(GameObject * object) : myObject(object) {
	}

	GameComponent::~GameComponent() {
	}

	void GameComponent::Dispatch(const MessageEvent & message) {
		if (myObject != nullptr & (message.HandleMode & MessageHandle_DispatchUpwards != 0))
			myObject->HandleMessage(message, this);
	}

	void GameComponent::SetEnabled(bool value) {
		if (isEnabled && !value) {
			isEnabled = false;
			OnDisabled();
		}
		else if (!isEnabled && value) {
			isEnabled = true;
			OnEnabled();
		}
	}

} }