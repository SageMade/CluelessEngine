#include "game_logic/GameObject.h"

namespace clueless { namespace game_logic {

	GameObject::GameObject(GameObject* parent) : GameComponent(parent), LocalTransform(Transform()), myComponents(TypeMap<GameComponent>()) {
		LocalTransform = Transform();
	}

	GameObject::~GameObject() {

	}

	void GameObject::PrePhysics() {
		for (int ix = 0; ix < myComponents.Count(); ix++) {
			myComponents.GetAt(ix)->PrePhysics();
		}
	}

	void GameObject::HandleMessage(const MessageEvent & message, GameComponent *dispatchedFrom) {

		if (message.HandleMode & MessageHandle_DispatchChildren) {
			for (int ix = 0; ix < myComponents.Count(); ix++)
				if (myComponents.GetAt(ix) != dispatchedFrom)
					myComponents.GetAt(ix)->HandleMessage(message);
		}

		if (message.HandleMode & MessageHandle_DispatchUpwards) {
			if (myObject)
				myObject->HandleMessage(message);
		}
	}

	void GameObject::OnStart() {
        for(int ix = 0; ix < myComponents.Count(); ix++) {
			myComponents.GetAt(ix)->OnStart();
		}
	}

	void GameObject::Render() {
        for(int ix = 0; ix < myComponents.Count(); ix++) {
			myComponents.GetAt(ix)->Render();
		}
	}

	void GameObject::AddedToScene() {
		for (int ix = 0; ix < myComponents.Count(); ix++) {
			myComponents.GetAt(ix)->AddedToScene();
		}
	}

	void GameObject::RemovedFromScene() {
		for (int ix = 0; ix < myComponents.Count(); ix++) {
			myComponents.GetAt(ix)->RemovedFromScene();
		}
	}

	void GameObject::Update() {
        for(int ix = 0; ix < myComponents.Count(); ix++) {
			myComponents.GetAt(ix)->Update();
		}
	}

	void GameObject::FixedUpdate() {
        for(int ix = 0; ix < myComponents.Count(); ix++) {
			myComponents.GetAt(ix)->FixedUpdate();
		}
	}

	void GameObject::LateUpdate() {
        for(int ix = 0; ix < myComponents.Count(); ix++) {
			myComponents.GetAt(ix)->LateUpdate();
		}
	}
	
	void GameObject::LookAt(glm::vec3 target)
	{
		// Sorry future me! Your problem now!
		//   ~ 6AM me
		glm::mat4 world = GetWorldTransform();
		world = glm::inverse(world);
		glm::vec4 target4 = glm::vec4(1.0f);
		target4.xyz = target;
		target4 = target4 * world;
		target = target4.xyz;
		glm::vec3 up = (glm::vec4(0.0f, 0.0f, 1.0f, 1.0f) * (glm::mat4)LocalTransform).xyz;
		LocalTransform.Orientation = glm::conjugate(glm::quat_cast(glm::lookAt(LocalTransform.Position, target, up)));
	}

	glm::mat4 GameObject::GetWorldTransform() {
		if (myObject)
			return (glm::mat4)LocalTransform * myObject->GetWorldTransform();
		else
			return LocalTransform;
	}
	
	void GameObject::SetWorldTransform(glm::mat4 transform) {

		//world = local * parent
		//local = world / paren;

		glm::mat4 result;

		if (myObject)
			result = transform * glm::inverse((glm::mat4)myObject->GetWorldTransform());
		else
			result = transform;

		glm::vec3 scale, skew;
		glm::vec4 perspective;
		glm::decompose(result, scale, LocalTransform.Orientation, LocalTransform.Position, skew, perspective);
	}

	glm::vec3 GameObject::ToLocalSpace(const glm::vec3 & worldValue) {
		return (glm::vec4(worldValue.x, worldValue.y, worldValue.z, 1.0f) * glm::inverse((glm::mat4)GetWorldTransform())).xyz;
	}

	glm::vec3 GameObject::ToLocalSpaceNorm(const glm::vec3 & worldNorm) {
		return (glm::vec3(worldNorm.x, worldNorm.y, worldNorm.z) * glm::inverse(glm::mat3(GetWorldTransform()))).xyz;
	}

	void GameObject::RotateLocalSpace(const glm::vec3& euler) {
		LocalTransform.Orientation *= glm::quat(euler);
	}

	void GameObject::RotateParentSpace(const glm::vec3 & euler) {
		LocalTransform.Orientation = glm::quat(euler) * LocalTransform.Orientation;
	}

} }