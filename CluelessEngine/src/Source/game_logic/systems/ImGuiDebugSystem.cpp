#include "game_logic/systems/ImGuiDebugSystem.h"

#include <imgui/imgui.h>
#include <imgui/imgui_impl.h>

#include "ui/input/InputManager.h"

#include "game_logic/Scene.h"

#include "game_logic/components/CameraController.h"

#include "graphics/DebugDrawer.h"
#include "graphics/Shader.h"

namespace clueless {
	namespace game_logic {
		namespace systems {

			using namespace ui::input;
			using namespace components;

			bool ImGuiDebugSystem::IsOpen = false;

			ui::TrueTypeFont *ImGuiDebugSystem::myFont;

			ImGuiDebugSystem::ImGuiDebugSystem() {

			}

			ImGuiDebugSystem::~ImGuiDebugSystem() {

			}

			void ImGuiDebugSystem::Init() {
				myFont = new ui::TrueTypeFont("res/fonts/Roboto-Medium.ttf", 32);
			}
			
			void ImGuiDebugSystem::PostRender() {

				ICamera* cam = Scene::GetActiveCamera();
				CameraController *controller = cam->GetParent()->GetComponent<CameraController>();
				
				if (IsOpen) {
					if (ImGui::Begin("Test", nullptr, ImGuiWindowFlags_AlwaysAutoResize)) {
						ImGui::Text("ImGui is working! :D");
						if (controller) {
							ImGui::DragFloat2("Sensitivity", &controller->Sensitivity[0], 0.01f, 0.0f, 1.0f);
							glm::vec2 hard = controller->MouseDelta;
							ImGui::DragFloat2("Mouse Dxy", &hard[0]);
							glm::vec3 loc = controller->GetParent()->LocalTransform.Position;
							ImGui::DragFloat3("Camera Location", &loc[0]);
						}
						static bool showDebug = true;
						ImGui::Checkbox("Debug", &showDebug);

						if (showDebug) {

							glEnable(GL_DEPTH_TEST);

							graphics::DebugDrawer::Singleton.setDebugMode(1);
							Scene::GetPhysicsWorld()->DebugDraw();

						}

						ImGui::End();
					}
				}
			}

		}
	}
}
