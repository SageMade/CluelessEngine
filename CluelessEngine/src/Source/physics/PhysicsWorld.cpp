#include "physics/PhysicsWorld.h"

#include "BulletCollision/CollisionDispatch/btGhostObject.h"

#include "GameTime.h"

#include "graphics/DebugDrawer.h"

#include <exception>

namespace clueless { namespace physics {

	PhysicsWorld::PhysicsWorld() {
		myCollisionConfig = new btDefaultCollisionConfiguration();
		myDispatcher = new btCollisionDispatcher(myCollisionConfig);
		myBroadphase = new btDbvtBroadphase();
		myBroadphase->getOverlappingPairCache()->setInternalGhostPairCallback(new btGhostPairCallback());
		mySolver = new btSequentialImpulseConstraintSolver;
		myWorld = new btDiscreteDynamicsWorld(myDispatcher, myBroadphase, mySolver, myCollisionConfig);
		myWorld->setGravity(btVector3(0, 0.0f, -9.81f));

		myWorld->setDebugDrawer(&graphics::DebugDrawer::Singleton);
	}

	PhysicsWorld::~PhysicsWorld()
	{
		delete myWorld;
		delete mySolver;
		delete myBroadphase;
		delete myDispatcher;
		delete myCollisionConfig;
	}
	
	void PhysicsWorld::AddRigidBody(RigidBody * body) {
		body->myWorld = this;
		myWorld->addRigidBody(body->myRigidBody, body->myCollisionGroup, body->myCollisionMask);
	}

	void PhysicsWorld::RemoveRigidBody(RigidBody * body) {
		body->myWorld = nullptr;
		myWorld->removeRigidBody(body->myRigidBody);
	}

	void PhysicsWorld::DebugDraw() {
		myWorld->debugDrawWorld();
	}
	
	void PhysicsWorld::SetGravity(glm::vec3 value) {
		btVector3 gravity = { value.x, value.y, value.z };
		myWorld->setGravity(gravity);
	}

	void PhysicsWorld::SetGravity(float x, float y, float z) {
		myWorld->setGravity(btVector3(x, y, z));
	}

	void PhysicsWorld::Update()
	{
		myWorld->stepSimulation(GameTime::FrameDeltaTime, 10);
	}

} }