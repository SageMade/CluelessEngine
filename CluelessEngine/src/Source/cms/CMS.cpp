#include "cms/CMS.h"

#include "graphics/CubeMap.h"
#include "graphics/Texture.h"
#include "graphics/Shader.h"
#include "graphics/Model.h"
#include "ui/TrueTypeFont.h"

#include <locale>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "utils/common.h"

ContentToken                   CMS::NullToken(TypeUnknown, 0);
std::vector<ContentToken>      CMS::myTokens;
ContentTagPtr                  CMS::myContent[CMS_TYPE_COUNT];
uint32_t                       CMS::myContentCounts[CMS_TYPE_COUNT];

std::map<std::string, Bundle*> CMS::myBundles;

using namespace clueless::graphics;
using namespace clueless::audio; 
using namespace clueless::ui;

bool ContentToken::GetIsLoaded() const {
	return TypeID != TypeUnknown & CMS::myContent[tType(TypeID)][ResourceID].Resource != nullptr; 
}

void CMS::Init() {

}

//unloads all bundles
void CMS::Unload() {
	
	// TODO unload all content

	// TODO delete all content tags

	// Delete all the tokens
}

ContentToken& CMS::GetToken(CmsType type, const std::string & name) {
	// Make sure name is lowercase
	std::string lowerName = lowerType(name);

	// Only look for a token if the type is valid
	if (type >= CMS_TYPE_MIN && type <= CMS_TYPE_MAX) {
		// Look at all the resources of that type
		for (int ix = 0; ix < myContentCounts[tType(type)]; ix++) {
			// Compare names, proceed if they are the same
			if (myContent[tType(type)][ix].Name == lowerName) {
				// Search existing tokens and return if one exists
				for (int j = 0; j < myTokens.size(); j++){
					if (myTokens[j].TypeID == type && myTokens[j].ResourceID == ix) {
						return myTokens[j];
					}
				}

				// No token exists, make one, add to list and return
				// We can safely assume content has not been loaded if it is the first token fetch
				ContentToken result(type, ix);
				myTokens.push_back(result);
				return result;
			}
		}
		// No matching names found, return a nullptr
		return NullToken;
	}
	else
		// Typecode invalid, return nullptr
		return NullToken;
}

void CMS::xmlTextureRead(Bundle& bundle, tinyxml2::XMLElement *document)
{
	//texture variables
	const char* filterMode;
	const char* wrap;
	const char* anti;
	
	//maps to change for textures filters and wraps
	std::map < std::string, GLenum > filters;
	filters.insert(std::pair<std::string, GLenum>("linear", GL_LINEAR));
	filters.insert(std::pair<std::string, GLenum>("nearest", GL_NEAREST));
	filters.insert(std::pair<std::string, GLenum>("nmipmapn", GL_NEAREST_MIPMAP_NEAREST));
	filters.insert(std::pair<std::string, GLenum>("lmipmapn", GL_LINEAR_MIPMAP_NEAREST));
	filters.insert(std::pair<std::string, GLenum>("lmipmapl", GL_LINEAR_MIPMAP_LINEAR));
	filters.insert(std::pair<std::string, GLenum>("nmipmapl", GL_NEAREST_MIPMAP_LINEAR));

	std::map<std::string, GLenum> wraps;
	wraps.insert(std::pair<std::string, GLenum>("repeat", GL_REPEAT));
	wraps.insert(std::pair<std::string, GLenum>("mrepeat", GL_MIRRORED_REPEAT));
	wraps.insert(std::pair<std::string, GLenum>("edge", GL_CLAMP_TO_EDGE));
	wraps.insert(std::pair<std::string, GLenum>("border", GL_CLAMP_TO_BORDER));
	
	//loads textures into file
	tinyxml2::XMLElement* textureElement = document->FirstChildElement("Texture");
	tinyxml2::XMLElement* childTextureElements;

	TextureMeta* temp;
	ContentTag *tempTag;

	while (textureElement)
	{
		//create new temp texture def
		temp = new TextureMeta();
		//create new content tag
		tempTag = new ContentTag;

		temp->FilterMode = GL_LINEAR;
		temp->WrapMode = GL_CLAMP_TO_EDGE;
		temp->EnableAnisotropy = false;

		//set the texture name, filelocation, typecode to texture
		tempTag->Name= textureElement->Attribute("name", 0);
		temp->FileName = textureElement->Attribute("src", 0);
		
		//checks filter, if exists
		childTextureElements = textureElement->FirstChildElement("FilterMode");
		if (childTextureElements)
		{
			//gets the filter mode
			filterMode = childTextureElements->Attribute("mode", 0);
			//sets the filterMode to what was inputed, via filters
			temp->FilterMode=filters.find(filterMode)->second;
		}


		//checks for wrapMode
		childTextureElements = textureElement->FirstChildElement("WrapMode");
		if (childTextureElements)
		{
			//get the wrap mode for the xml
			wrap = childTextureElements->Attribute("mode", 0);
			//set the temp texture def wrap
			temp->WrapMode = wraps.find(wrap)->second;
		}

		//checks for AnisotropyEnabled
		childTextureElements = textureElement->FirstChildElement("AnisotropyEnabled");
		if (childTextureElements)
		{
			temp->EnableAnisotropy = true;
		}
		//sets the meta data to the texture data 
		tempTag->MetaData = (void*)temp;


		ContentToken &token = Register(TypeTexture, tempTag->Name, nullptr, (void*)temp);

		if (!token.IsNull())
			bundle.Content.insert(std::pair<std::string, ContentToken>(tempTag->Name, token));
		else {
			FILE_LOG(logWARNING) << "Failed to register texture resource with name \"" << tempTag->Name << "\". Possible redefinition?";
		}

		//go to next element with texture
		textureElement = textureElement->NextSiblingElement("Texture");
	}
}

void CMS::xmlSoundLoad(Bundle& bundle, tinyxml2::XMLElement *document)
{	
	// loads sound tags from file
	tinyxml2::XMLElement* soundElement = document->FirstChildElement("SoundEffect");

	SoundMeta* temp;
	std::string name;

	while (soundElement)
	{
		//create new temp texture def
		temp = new SoundMeta();

		//set the texture name, filelocation, typecode to texture
		name = soundElement->Attribute("name", 0);
		temp->FileName = soundElement->Attribute("src", 0);

		temp->Pan = soundElement->FloatAttribute("pan", 0.0f);
		temp->Pitch = soundElement->FloatAttribute("pitch", 1.0f);
		temp->Gain = soundElement->FloatAttribute("gain", 1.0f);
				
		ContentToken &token = Register(TypeSound, name, nullptr, (void*)temp);

		if (!token.IsNull())
			bundle.Content.insert(std::pair<std::string, ContentToken>(name, token));
		else {
			FILE_LOG(logWARNING) << "Failed to register sound resource with name \"" << name << "\". Possible redefinition?";
		}

		//go to next element with texture
		soundElement = soundElement->NextSiblingElement("SoundEffect");
	}
}

void CMS::xmlShaderLoad(Bundle& bundle, tinyxml2::XMLElement *document)
{
	//loads textures into file
	tinyxml2::XMLElement* element = document->FirstChildElement("Shader");
	tinyxml2::XMLElement* childElement;

	ShaderMeta* temp;
	std::string name;

	while (element)
	{
		//create new temp texture def
		temp = new ShaderMeta();

		//set the texture name, filelocation, typecode to texture
		name = element->Attribute("name", 0);

		childElement = element->FirstChildElement("VsSource");
		if (childElement)
		{
			const char* tempText = childElement->Attribute("src", 0);
			if (tempText) {
				temp->VsFileName = tempText;
			}
			else {
				temp->VsSource = childElement->GetText();
			}
		}

		childElement = element->FirstChildElement("FsSource");
		if (childElement)
		{
			const char* tempText = childElement->Attribute("src", 0);
			if (tempText) {
				temp->FsFileName = tempText;
			}
			else {
				temp->FsSource = childElement->GetText();
			}
		}

		childElement = element->FirstChildElement("GsSource");
		if (childElement)
		{
			const char* tempText = childElement->Attribute("src", 0);
			if (tempText) {
				temp->GsFileName = tempText;
			}
			else {
				temp->GsSource = childElement->GetText();
			}
		}

		if ((temp->VsSource == "" && temp->VsFileName == "") ||
			(temp->FsSource == "" && temp->FsFileName == "")) {
			FILE_LOG(logWARNING) << "Shader \"" << name << "\" has not been fully defined";
		}
		else {
			ContentToken token = Register(TypeShader, name, nullptr, (void*)temp);

			if (!token.IsNull())
				bundle.Content.insert(std::pair<std::string, ContentToken>(name, token));
			else {
				FILE_LOG(logWARNING) << "Failed to register shader resource with name \"" << name << "\". Possible redefinition?";
			}
		}

		//go to next element with texture
		element = element->NextSiblingElement("Shader");
	}
}

void CMS::xmlCubeMapLoad(Bundle& bundle, tinyxml2::XMLElement *document) {
	//loads textures into file
	tinyxml2::XMLElement* element = document->FirstChildElement("CubeMap");
	tinyxml2::XMLElement* childElement;

	CubeMapMeta* temp;
	std::string name;

	while (element)
	{
		//create new temp texture def
		temp = new CubeMapMeta();

		//set the texture name, filelocation, typecode to texture
		name = element->Attribute("name", 0);

		childElement = element->FirstChildElement("NegX");
		if (childElement)
			temp->NegXPath = childElement->Attribute("src", 0);

		childElement = element->FirstChildElement("PosX");
		if (childElement)
			temp->PosXPath = childElement->Attribute("src", 0);

		childElement = element->FirstChildElement("NegY");
		if (childElement)
			temp->NegYPath = childElement->Attribute("src", 0);

		childElement = element->FirstChildElement("PosY");
		if (childElement)
			temp->PosYPath = childElement->Attribute("src", 0);

		childElement = element->FirstChildElement("NegZ");
		if (childElement)
			temp->NegZPath = childElement->Attribute("src", 0);

		childElement = element->FirstChildElement("PosZ");
		if (childElement)
			temp->PosZPath = childElement->Attribute("src", 0);

		if (temp->NegXPath == "" || temp->PosXPath == "" || temp->NegYPath == "" || temp->PosYPath == "" ||
			temp->NegZPath == "" || temp->PosZPath == "") {
			FILE_LOG(logWARNING) << "CubeMap \"" << name << "\" has not been fully defined";
		}
		else {
			ContentToken &token = Register(TypeCubeMap, name, nullptr, (void*)temp);

			if (!token.IsNull())
				bundle.Content.insert(std::pair<std::string, ContentToken>(name, token));
			else {
				FILE_LOG(logWARNING) << "Failed to register cubemap resource with name \"" << name << "\". Possible redefinition?";
			}
		}

		//go to next element with texture
		element = element->NextSiblingElement("CubeMap");
	}
}

void CMS::xmlModelLoad(Bundle& bundle, tinyxml2::XMLElement * document)
{
	//loads textures into file
	tinyxml2::XMLElement* element = document->FirstChildElement("Model");
	tinyxml2::XMLElement* childElement;

	ModelMeta* temp;
	std::string name;

	while (element)
	{
		//create new temp texture def
		temp = new ModelMeta();

		//set the texture name, filelocation, typecode to texture
		name = element->Attribute("name", 0);

		childElement = element->FirstChildElement("SourceFile");
		if (childElement) {
			temp->SourceFile = childElement->GetText();
			const char * select = childElement->Attribute("selector", 0);
			if (select == nullptr)
				temp->NodeSelector = "root";
			else
				temp->NodeSelector = select;
		}
				
		if (temp->SourceFile == "") {
			FILE_LOG(logWARNING) << "Model \"" << name << "\" has not been fully defined";
		}
		else {
			ContentToken &token = Register(TypeModel, name, nullptr, (void*)temp);

			if (!token.IsNull())
				bundle.Content.insert(std::pair<std::string, ContentToken>(name, token));
			else {
				FILE_LOG(logWARNING) << "Failed to register model resource with name \"" << name << "\". Possible redefinition?";
			}
		}

		//go to next element with texture
		element = element->NextSiblingElement("Model");
	}
}

void CMS::xmlTextLoad(Bundle& bundle, tinyxml2::XMLElement * document)
{
	//loads textures into file
	tinyxml2::XMLElement* element = document->FirstChildElement("Text");
	tinyxml2::XMLElement* childElement;

	TextMeta* temp;
	std::string name;

	while (element)
	{
		//create new temp texture def
		temp = new TextMeta();

		//set the texture name, filelocation, typecode to texture
		name = element->Attribute("name", 0);

		childElement = element->FirstChildElement("SourceFile");
		if (childElement) {
			temp->SourcePath = childElement->Attribute("src", 0);
			if (temp->SourcePath == "") {
				temp->Text = childElement->GetText();
			}
		}

		if (temp->SourcePath == "" && temp->Text == nullptr) {
			FILE_LOG(logWARNING) << "Text block \"" << name << "\" has not been fully defined";
		}
		else {
			ContentToken &token = Register(TypeText, name, nullptr, (void*)temp);

			if (!token.IsNull())
				bundle.Content.insert(std::pair<std::string, ContentToken>(name, token));
			else {
				FILE_LOG(logWARNING) << "Failed to register text resource with name \"" << name << "\". Possible redefinition?";
			}
		}

		//go to next element with texture
		element = element->NextSiblingElement("CubeMap");
	}
}

void CMS::xmlMaterialLoad(Bundle & bundle, tinyxml2::XMLElement * document) {
	//loads textures into file
	tinyxml2::XMLElement* element = document->FirstChildElement("Material");
	tinyxml2::XMLElement* childElement;

	MaterialMeta* temp;
	std::string name;

	while (element)
	{
		//create new temp texture def
		temp = new MaterialMeta();

		//set the texture name, filelocation, typecode to texture
		name = element->Attribute("name", 0);

		childElement = element->FirstChildElement("Shader");
		if (childElement) {
			temp->ShaderName = childElement->GetText();			
		}

		struct MaterialTextureTypeMap : public std::map<std::string, int>
		{
			MaterialTextureTypeMap()
			{
				this->operator[]("diffuse")  = 0;
				this->operator[]("specular") = 1;
				this->operator[]("emissive") = 2;
				this->operator[]("normal")   = 3;
			};
			~MaterialTextureTypeMap() {}
		};

		static MaterialTextureTypeMap matTypes;

		childElement = element->FirstChildElement("Texture");
		while (childElement != nullptr) {
			const char* slot = childElement->Attribute("slot");
			const char* src = childElement->Attribute("name");

			if (slot != nullptr & src != nullptr) {
				std::string slotStr = slot;
				std::transform(slotStr.begin(), slotStr.end(), slotStr.begin(), ::tolower);

				auto it = matTypes.find(slotStr);
				if (it != matTypes.end()) {
					temp->TextureNames[it->second] = src;
				}
			}
			childElement = childElement->NextSiblingElement("Texture");
		}
		
		ContentToken &token = Register(TypeMaterial, name, nullptr, (void*)temp);

		if (!token.IsNull())
			bundle.Content.insert(std::pair<std::string, ContentToken>(name, token));
		else {
			FILE_LOG(logWARNING) << "Failed to register material with name \"" << name << "\". Possible redefinition?";
		}

		//go to next element with texture
		element = element->NextSiblingElement("Material");
	}
}

void CMS::xmlFontLoad(Bundle & bundle, tinyxml2::XMLElement * document) {
	//loads textures into file
	tinyxml2::XMLElement* element = document->FirstChildElement("Font");
	tinyxml2::XMLElement* childElement;

	TrueTypeFontMeta* temp;
	std::string name;

	while (element)
	{
		//create new temp texture def
		temp = new TrueTypeFontMeta();

		//set the texture name, filelocation, typecode to texture
		name = element->Attribute("name", 0);

		const char* source = element->Attribute("src");
		int         size = element->IntAttribute("size", 16);

		if (source) {
			temp->FileName = source;
			temp->Size = size;

			ContentToken &token = Register(TypeFont, name, nullptr, (void*)temp);

			if (!token.IsNull())
				bundle.Content.insert(std::pair<std::string, ContentToken>(name, token));
			else {
				FILE_LOG(logWARNING) << "Failed to register font with name \"" << name << "\". Possible redefinition?";
			}

		}
		else {
			FILE_LOG(logWARNING) << "Failed to register font with name \"" << name << "\". Incomplete structure";
		}

		//go to next element with texture
		element = element->NextSiblingElement("Font");
	}
}

std::string CMS::lowerType(std::string type)
{
	static std::locale loc("");
	std::string content = "";
	for (int i = 0; i < type.size(); i++)
	{
		content += std::tolower(type[i], loc);
	}
	return content;
}

void CMS::__LoadResource(const ContentToken& token) {
	ContentTag& resource = myContent[tType(token.TypeID)][token.ResourceID];

	if (resource.MetaData == nullptr) {
		resource.Resource = nullptr;

		FILE_LOG(logWARNING) << "Failed to load resource with null content";
		return;
	}

	switch (token.TypeID) {
		case TypeTexture: {
				TextureMeta *metaData = (TextureMeta*)resource.MetaData;
				Texture* result = new Texture(metaData->FileName.c_str(), metaData->FilterMode, metaData->WrapMode, metaData->EnableAnisotropy);
				resource.Resource = result;
			}
			break;
		case TypeSound: {
				SoundMeta *metaData = (SoundMeta*)resource.MetaData;
				SoundEffect* result = new SoundEffect(resource.Name, metaData->FileName.c_str());
				result->SetPitch(metaData->Pitch);
				result->SetPan(metaData->Pan);
				result->SetGain(metaData->Gain);
				resource.Resource = result;
			}
			break;
		case TypeShader: {
				ShaderMeta *metaData = (ShaderMeta*)resource.MetaData;
				Shader* result = new Shader();

				if (metaData->VsFileName != "")
					result->LoadShaderPart(GL_VERTEX_SHADER, metaData->VsFileName.c_str());
				else
					result->LoadShaderPartFromSource(GL_VERTEX_SHADER, metaData->VsSource.c_str());

				if (metaData->FsFileName != "")
					result->LoadShaderPart(GL_FRAGMENT_SHADER, metaData->FsFileName.c_str());
				else
					result->LoadShaderPartFromSource(GL_FRAGMENT_SHADER, metaData->FsSource.c_str());

				if (metaData->GsFileName != "")
					result->LoadShaderPart(GL_GEOMETRY_SHADER, metaData->GsFileName.c_str());
				else if (metaData->GsSource != "")
					result->LoadShaderPartFromSource(GL_GEOMETRY_SHADER, metaData->GsSource.c_str());

				result->Link();
				resource.Resource = result;
			}
		    break;
		case TypeCubeMap: {
				CubeMapMeta *metaData = (CubeMapMeta*)resource.MetaData;
				
				const char **paths = new const char*[6]{
					metaData->NegZPath.c_str(),
					metaData->PosZPath.c_str(),
					metaData->NegYPath.c_str(),
					metaData->PosYPath.c_str(),
					metaData->NegXPath.c_str(),
					metaData->PosXPath.c_str(),
				};

				CubeMap *result = new CubeMap(paths);
				resource.Resource = result;
			}
			break;
		case TypeUser: {
				UserMeta *metaData = (UserMeta*)resource.MetaData;
				void* result = metaData->LoadContent(metaData->MetaData);
				resource.Resource = result;
			}
			break;
		case TypeModel: {
				ModelMeta* metaData = (ModelMeta*)resource.MetaData;

				Assimp::Importer importer;
				const aiScene *scene = importer.ReadFile(metaData->SourceFile, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);//, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
																																									   // check for errors
				if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) // if is Not Zero
				{
					FILE_LOG(logWARNING) << "ERROR::ASSIMP:: " << importer.GetErrorString();
				}
				if (metaData->NodeSelector == "root") {
					resource.Resource = (void*)Model::LoadFromNode(scene, scene->mRootNode);
				}
				else {
					aiNode *node = scene->mRootNode;
					int startX = 0;
					//for (int ix = 0; ix < metaData->NodeSelector.length(); ix++) {
						//if (metaData->NodeSelector[ix] == '.') 
						{
						aiString path = aiString(metaData->NodeSelector);//.substr(startX, ix - startX));
							//startX = ix + 1;

							for (int j = 0; j < node->mNumChildren; j++) {
								if (node->mChildren[j]->mName == path) {
									node = node->mChildren[j];
									break;
								}
							}
						}
					//}
					resource.Resource = (void*)Model::LoadFromNode(scene, node);
				}
			}
			break;
		case TypeText: {
				TextMeta* metaData = (TextMeta*)resource.MetaData;
				if (metaData->SourcePath != "") {
					resource.Resource = clueless::readFile(metaData->SourcePath.c_str());
				}
				else {
					resource.Resource = (void*)metaData->Text;
				}
			}
			break;
		case TypeMaterial: {
				MaterialMeta *metaData = (MaterialMeta*)resource.MetaData;
				
				Material *result = new Material();
				if (metaData->TextureNames[0])
					result->SetTexture(CMS::RedeemDirect<Texture>(CmsType::TypeTexture, metaData->TextureNames[0]), TextureUsage::TextureDiffuse);
				if (metaData->TextureNames[1])
					result->SetTexture(CMS::RedeemDirect<Texture>(CmsType::TypeTexture, metaData->TextureNames[1]), TextureUsage::TextureSpecular);
				if (metaData->TextureNames[2])
					result->SetTexture(CMS::RedeemDirect<Texture>(CmsType::TypeTexture, metaData->TextureNames[2]), TextureUsage::TextureEmissive);
				if (metaData->TextureNames[3])
					result->SetTexture(CMS::RedeemDirect<Texture>(CmsType::TypeTexture, metaData->TextureNames[3]), TextureUsage::TextureNormal);
				result->SetShaderToken(CMS::GetToken(CmsType::TypeShader, metaData->ShaderName));
				result->LoadIdentity();

				resource.Resource = (void*)result;
			}
			break;
		case TypeFont: {
				TrueTypeFontMeta *metaData = (TrueTypeFontMeta*)resource.MetaData;
				TrueTypeFont *result = new TrueTypeFont(metaData->FileName.c_str(), metaData->Size);
				resource.Resource = result;
			}
			break;
	}
} 

void CMS::__UnloadResource(const ContentToken& token) {
	ContentTag& resource = myContent[tType(token.TypeID)][token.ResourceID];

	if (resource.Resource == nullptr) {
		FILE_LOG(logWARNING) << "Trying to unload null resource. Possibly has already been unloaded";
		return;
	}

	switch (token.TypeID) {
		case TypeTexture: {
				Texture *texture = (Texture*)resource.Resource;
				texture->Unload();
				delete texture;
				resource.Resource = nullptr;
			}
			break;
		case TypeSound: {
				SoundEffect *sound = (SoundEffect*)resource.Resource;
				sound->Unload();
				delete sound;
				resource.Resource = nullptr;
			}
			break;
		case TypeShader: {
				Shader *shader = (Shader*)resource.Resource;
				shader->Unload();
				delete shader;
				resource.Resource = nullptr;
			}
			break;
		case TypeCubeMap: {
				CubeMap *cubeMap = (CubeMap*)resource.Resource;
				cubeMap->Unload();
				delete cubeMap;
				resource.Resource = nullptr;
			}
		    break;
		case TypeMaterial: {
				Material *material = (Material*)resource.Resource;
				material->Unload();
				delete material;
				resource.Resource = nullptr;
			}
			break;
		case TypeFont: {
				TrueTypeFont *font = (TrueTypeFont*)resource.Resource;
				font->Unload();
				delete font;
				resource.Resource = nullptr;
			}
			break;
		case TypeUser: {
				// Hopefully they passed a metadata that shows us how to unload
				if (resource.MetaData != nullptr) {
					// Grab dat metadata
					UserMeta *metaData = (UserMeta*)resource.MetaData;
					// Call the unload function and hope it unloads
					resource.Resource = metaData->UnloadContent(metaData->MetaData);
				}
				// Shiiit... hopefully we can find a way to unload
				else {
					// Go grab second integer from resource (first should be a ptr to the virtual functions table)
					uint32_t valid = *(uint32_t*)((char*)resource.Resource + sizeof(size_t));

					// If the integer is our resource identifier token, then we have hope yet!
					if (valid == RESOURCE_VALID_TOKEN) {
						FILE_LOG(logDEBUG) << "Attempting to delete loose-defined resource";
						// Cast the resource to an IResource and unload
						IResource* cRes = (IResource*)resource.Resource;
						cRes->Unload();
						// Because of the virtual destructor in IResource, should work OK
						delete cRes;
						// Nullify the resource handle and update the isLoaded
						resource.Resource = nullptr;
						FILE_LOG(logDEBUG) << "Success!";
					}
					else {
						// No other way around it... :(
						FILE_LOG(logDEBUG) << "Cannot find destructor for loose-defined content with name \"" << resource.Name << "\", will not clean up";
					}
				}
			}
			break;
	}
}

void CMS::Unload(const ContentToken& token) {
	if (token.GetIsLoaded()) {
		__UnloadResource(token);
	}
}

void CMS::ForceLoad(const ContentToken& token) {
	__LoadResource(token);
}

ContentToken CMS::Register(CmsType type, const std::string & name, void * resource, void * metaData) {
	// Make sure name is lowercase
	std::string lowerName = lowerType(name);

	// Only look for a token if the type is valid
	if (type >= CMS_TYPE_MIN && type <= CMS_TYPE_MAX) {
		// Look at all the resources of that type
		for (int ix = 0; ix < myContentCounts[tType(type)]; ix++) {
			// Compare names, if we have something under the same name, return a nullptr
			if (myContent[tType(type)][ix].Name == lowerName)
				return NullToken;
		}

		// Allocate a new array to store the content tags in, copy data, delete old and update pointer
		ContentTag *newArray = new ContentTag[myContentCounts[tType(type)] + 1];
		for (int ix = 0; ix < myContentCounts[tType(type)]; ix++)
			newArray[ix] = myContent[tType(type)][ix];
		delete[] myContent[tType(type)];
		myContent[tType(type)] = newArray; 

		// Set up the content tag for the new resource
		myContent[tType(type)][myContentCounts[tType(type)]] = ContentTag();
		myContent[tType(type)][myContentCounts[tType(type)]].Name = lowerName;
		myContent[tType(type)][myContentCounts[tType(type)]].Resource = resource;
		myContent[tType(type)][myContentCounts[tType(type)]].MetaData = metaData;
		
		// Build a token, push it to the cleanup list, increment the content counts and return
		ContentToken result(type, myContentCounts[tType(type)]);
		myTokens.push_back(result);
		myContentCounts[tType(type)] ++;
		return result;
	}
	else
		// Typecode invalid, return nullptr
		return NullToken;
}

bool CMS::LoadManifest(const std::string& fileName)
{
	//manifest variables
	const char* bundleName;
	//loads xml file
	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument;
	doc->LoadFile(fileName.c_str());

	tinyxml2::XMLElement *manifestRoot = doc->FirstChildElement("Manifest");

	if (!manifestRoot) {
		return false;
	}

	//gets the first bundle and checks to see if it exists, if not return false
	tinyxml2::XMLElement *bundles = manifestRoot->FirstChildElement("Bundle");
	if (!bundles)
	{
		return false;
	}
 
	while (bundles)
	{
		bundleName = bundles->Attribute("name", 0);
		//create a new bundle
		Bundle* bundle = new Bundle(bundleName);
		
		myBundles.insert(std::pair<std::string, Bundle*>(bundleName, bundle));

		//start loading of stuff in blender
	    xmlTextureRead(*bundle, bundles);
	    xmlSoundLoad(*bundle, bundles);
	    xmlShaderLoad(*bundle, bundles);
	    xmlCubeMapLoad(*bundle, bundles);
	    xmlModelLoad(*bundle, bundles);
		xmlTextLoad(*bundle, bundles);
		xmlMaterialLoad(*bundle, bundles);
		xmlFontLoad(*bundle, bundles);

		//get the next bundle to load
		bundles = bundles->NextSiblingElement("Bundle");
	}
}

/* 
	Loads a bundle of resources into memory
	@param bundleName The name of the bundle to load
*/
void CMS::LoadBundle(const std::string& bundleName)
{
	//structs
	if (myBundles.find(bundleName)!=myBundles.end())
	{
		Bundle *bundle = myBundles.find(bundleName)->second;
		//loads bundle
		for (auto& x : bundle->Content)
		{
			__LoadResource(x.second);
		}
	}
}
/* 
	Unloads a bundle's resources from memory
	@params bundleName Name of the bundle to load
*/
void CMS::UnloadBundle(const std::string& bundleName)
{
	//structs
	if (myBundles.find(bundleName) != myBundles.end())
	{
		Bundle *bundle = myBundles.find(bundleName)->second;
		//loads bundle
		for (auto& x : bundle->Content)
		{
			__UnloadResource(x.second);
		}
	}
}