#include "anim/Animate.h"

namespace clueless {
	namespace anim {
		const glm::mat4 Animator::catmulRomMatrix = {
			-0.5f,  1.5f, -1.5f,  0.5f,
			1.0f, -2.5f,  2.0f, -0.5f,
			-0.5f,  0.0f,  0.5f,  0.0f,
			0.0f,  1.0f,  0.0f,  0.0f
		};

		const glm::mat4 Animator::bezierMatrix = {
			-1.0f,  3.0f, -3.0f,  1.0f,
			3.0f, -6.0f,  3.0f, -0.0f,
			-3.0f,  3.0f,  0.0f,  0.0f,
			1.0f,  0.0f,  0.0f,  0.0f
		};

		void AnimChannel::Resolve(float *data, float time) {

			AnimSegment* channelSegment = SegmentAt(time);

			if (channelSegment != nullptr) {
				float t = time - channelSegment->start.time;
				t /= channelSegment->end.time - channelSegment->start.time;

				data += Offset;

				// switch on channel type and animate accordingly

				float startVal, endVal;
				startVal = channelSegment->start.value;
				endVal = channelSegment->end.value;

				switch (channelSegment->type) {
				case 0: // LERP
					*data = Animator::Linear(startVal, endVal, t);
					break;
				case 1: // catmul-rom
					*data = Animator::CatmulRom(channelSegment->controls, startVal, endVal, t);
					break;
				case 2: // bezier
					*data = Animator::Bezier(channelSegment->controls, startVal, endVal, t);
					break;
				}
			}
		}
	}
}