#include "vr/VrSubSystem.h"

using namespace vr;

namespace clueless { namespace virtual_reality {

    using namespace vr;

    TrackedDevice *VrSubSystem::myTrackedDevices;
    uint32_t       VrSubSystem::myDeviceCount;
    HMD           *VrSubSystem::myHmd;
    IVRSystem     *VrSubSystem::myInternalSystem;

    void VrSubSystem::Init(){

        HmdError error;
        VrSubSystem::myInternalSystem = VR_Init(&error, EVRApplicationType::VRApplication_Scene);

        if (error) {
            throw vr_exception("Failed to inititalize VR subsystem");
        }

    }

    void VrSubSystem::Cleanup(){
        VR_Shutdown();
    }

} }
