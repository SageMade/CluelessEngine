#include "GraphicsConfig.h"

uint16_t GraphicsConfig::BackBufferHeight = 1080;
uint16_t GraphicsConfig::BackBufferWidth = 1920;
uint16_t GraphicsConfig::ScreenHeight = 1080;
uint16_t GraphicsConfig::ScreenWidth = 1920;
