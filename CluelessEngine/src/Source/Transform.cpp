#include "Transform.h"

namespace clueless {

	//Transform::Transform(glm::mat4 source) {
	//	static glm::vec3 skew;
	//	static glm::vec4 projection;
	//	glm::decompose(source, Scale, Orientation, Position, skew, projection);
	//}

	void Transform::CopyFrom(const btTransform& other) {
		CopyFromBullet(Position, other.getOrigin());
		CopyFromBullet(Orientation, other.getRotation());
	}

	void Transform::CopyTo(btTransform & other)
	{
		other.setOrigin(glm2bt(Position));
		other.setRotation(glm2bt(Orientation));
	}

	Transform::operator glm::mat4() const {
		glm::mat4 result = glm::translate(Position) * glm::mat4_cast(Orientation) *  glm::scale(Scale);
		return result;
	}

	glm::mat4 Transform::operator *(const Transform& other) {
		return (glm::mat4)(*this) * (glm::mat4)other;
	}
}