
#include "../Headers/ImGuiPuzzleSystem.h"

#include <imgui/imgui.h>
#include <imgui/imgui_impl.h>
#include <imgui/imgui_internal.h>
#include "ui/input/InputManager.h"
#include "game_logic/components/RigidBodyComponent.h";
#include "game_logic/components/TriggerColliderComponent.h";
#include "game_logic/components/CharacterController.h"
#include "game_logic/components/MorphMeshRenderComponent.h"


#include "game_logic/Scene.h"
#include "game_logic/components/CameraController.h"


#include "graphics/DebugDrawer.h"
#include "Questbook.h"

#include <iostream>
namespace clueless {
	namespace game_logic {
		namespace systems {
			using namespace ui::input;
			using namespace components;

			bool ImGuiPuzzleSystem::openPuzzle=false;
			bool ImGuiPuzzleSystem::switch1 = false;
			 bool ImGuiPuzzleSystem::switch2=false;
			bool ImGuiPuzzleSystem::switch3=false;
			bool ImGuiPuzzleSystem::switch4=false;
			 bool ImGuiPuzzleSystem::switch5=false;
			 int ImGuiPuzzleSystem:: puzzleNum=1;
			 bool  ImGuiPuzzleSystem::result = false;

			 std::map<std::string, GameObject *> ImGuiPuzzleSystem:: myObjects;

			 

			ImGuiPuzzleSystem::ImGuiPuzzleSystem()
			{
				
			}
			ImGuiPuzzleSystem::~ImGuiPuzzleSystem()
			{

			}

			void ImGuiPuzzleSystem::AddGameObject(std::string name, GameObject* object)
			{
				myObjects.insert(std::pair<std::string, GameObject*>(name, object));
			}


			void ImGuiPuzzleSystem::Reset()
			{

			}

			void ImGuiPuzzleSystem::Init()
			{

			}

			void ImGuiPuzzleSystem:: PrePhysics()
			{
				if (result &&puzzleNum == 1)
				{
					
					std::map<std::string, GameObject*>::iterator it = myObjects.find("door");
						if (it!=myObjects.end())
						{
							if (sound!=nullptr)
							{
								sound->Play();

							}

							std::map<std::string, GameObject*>::iterator it4 = myObjects.find("doorAnimation");
							if (it4 != myObjects.end())
							{
								MorphMeshRenderComponent *animation = it4->second->GetComponent<MorphMeshRenderComponent>();
								graphics::MorphMesh* mesh = animation->getMorphMesh();
								mesh->pauseLoop(false);
								mesh->SetLoopMode(anim::LoopType::LoopType_Stop);
							}
							switch1 = false;
							switch2 = false;
							switch3 = false;
							switch4 = false;
							switch5 = false;

							PuzzleStart ps;
							ps.flags = COMPLETE;
							ps.id = 1;
							Event temp;
							temp.type = Puzzle;
							temp.data = (void*)&ps;

							Questbook::Update(temp);

							GameObject* myObject = it->second;
							trans.Position = glm::vec3(0.0f, 0.0f, 10.0f);
							myObject->GetComponent<RigidBodyComponent>()->SetTransform(trans);

							TriggerColliderComponent* trigger = myObject->GetComponent<TriggerColliderComponent>();
							if (trigger)
								trigger->SetCallback([](GameObject*) {});

							CameraController* ctrl = Scene::GetActiveCamera()->GetParent()->GetComponent<CameraController>();
							ctrl->ToggleMouseLook();

							CharacterController::disabled = false;
							openPuzzle = false;
							result = false;
						}
					
					

				}


				if (result &&puzzleNum == 2)
				{

					std::map<std::string, GameObject*>::iterator it = myObjects.find("elevator");
					if (it != myObjects.end())
					{

						if (sound != nullptr)
						{
							sound->Play();
						}
						std::map<std::string, GameObject*>::iterator it2 = myObjects.find("debris");
						if (it2!=myObjects.end())
						{
							RigidBodyComponent *debris=it2->second->GetComponent<RigidBodyComponent>();
							debris->getBody()->GetInternalBody()->setLinearFactor(btVector3(1.0f, 1.0f, 1.0f));
							debris->getBody()->GetInternalBody()->setAngularFactor(btVector3(1.0f, 1.0f, 1.0f));
						}

						std::map<std::string, GameObject*>::iterator it3 = myObjects.find("elevatorAnimation");
						if (it3 != myObjects.end())
						{
							MorphMeshRenderComponent *animation = it3->second->GetComponent<MorphMeshRenderComponent>();
							graphics::MorphMesh* mesh = animation->getMorphMesh();
						    mesh->pauseLoop(false);
						}
						switch1 = false;
						switch2 = false;
						switch3 = false;
						switch4 = false;
						switch5 = false;

						PuzzleStart ps;
						ps.flags = COMPLETE;
						ps.id = 1;
						Event temp;
						temp.type = Puzzle;
						temp.data = (void*)&ps;

						Questbook::Update(temp);

						GameObject* myObject = it->second;
						trans.Position = glm::vec3(0.0f, 0.0f, 10.0f);
						//myObject->GetComponent<RigidBodyComponent>()->SetTransform(trans);

						TriggerColliderComponent* trigger = myObject->GetComponent<TriggerColliderComponent>();
						if (trigger)
							trigger->SetCallback([](GameObject*) {});

						CameraController* ctrl = Scene::GetActiveCamera()->GetParent()->GetComponent<CameraController>();
						ctrl->ToggleMouseLook();

						CharacterController::disabled = false;
						openPuzzle = false;
						result = false;
					}



				}

				if (result &&puzzleNum == 3)
				{
					if (myObjects.find("communications") != myObjects.end())
					{

						if (sound != nullptr)
						{
							sound->Play();
						}

						(*soundSet)["alarm"]->Stop();

						switch1 = false;
						switch2 = false;
						switch3 = false;
						switch4 = false;
						switch5 = false;
						std::map<std::string, GameObject*>::iterator it;
						it = myObjects.find("debris");
						if (it != myObjects.end())
						{
							RigidBodyComponent *debris = it->second->GetComponent<RigidBodyComponent>();
							debris->getBody()->GetInternalBody()->setLinearFactor(btVector3(0.0f, 0.0f, 0.0f));
							debris->getBody()->GetInternalBody()->setAngularFactor(btVector3(0.0f,0.0f, 0.0f));
						}

						GameObject* myObject = myObjects.find("communications")->second;
						
						TriggerColliderComponent* trigger = myObject->GetComponent<TriggerColliderComponent>();
						if (trigger)
							trigger->SetCallback([](GameObject*){});

						PuzzleStart ps;
						ps.flags = COMPLETE;
						ps.id = 1;
						Event temp;
						temp.type = Puzzle;
						temp.data = (void*)&ps;

						Questbook::Update(temp);
						CameraController* ctrl = Scene::GetActiveCamera()->GetParent()->GetComponent<CameraController>();
						ctrl->ToggleMouseLook();

						CharacterController::disabled = false;
						openPuzzle = false;
						result = false;
					}

				}
					
				
			}


			void ImGuiPuzzleSystem::FixedUpdate(GameObject * object)
			{

			

				
			}

			void ImGuiPuzzleSystem:: puzzle1(ImDrawList* drawList, ImRect bb)
			{

				std::vector<Switch> switches = puzzle->switches;
				std::vector<andGate> and = puzzle->and;
				std::vector<OrGate> or = puzzle-> or ;
				std::vector<XorGate> xor = puzzle-> xor;
				std::vector<Diode> diode = puzzle->diode;
				//switch 1
				drawList->AddRectFilled(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 5.0f),
					ImVec2(bb.Min.x + 45.0f, bb.Min.y + 25.0f),
					(switch1 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));
				drawList->AddText(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 5.0f), ImColor(255.0f, 255.0f, 0.0f, 255.0f),
					"SW1");
				//switch 2
				drawList->AddRectFilled(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 60.0f),
					ImVec2(bb.Min.x + 45.0f, bb.Min.y + 80.0f),
					(switch2 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));
				drawList->AddText(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 60.0f), ImColor(255.0f, 255.0f, 0.0f, 255.0f),
					"SW2");
				//switch 3
				drawList->AddRectFilled(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 115.0f),
					ImVec2(bb.Min.x + 45.0f, bb.Min.y + 135.0f),
					(switch3 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));
				drawList->AddText(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 115.0f), ImColor(255.0f, 255.0f, 0.0f, 255.0f),
					"SW3");
				////switch 4
				drawList->AddRectFilled(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 170.0f),
					ImVec2(bb.Min.x + 45.0f, bb.Min.y + 190.0f),
					(switch4 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));
				drawList->AddText(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 170.0f), ImColor(255.0f, 255.0f, 0.0f, 255.0f),
					"SW4");
				//switch 5
				drawList->AddRectFilled(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 225.0f),
					ImVec2(bb.Min.x + 45.0f, bb.Min.y + 245.0f),
					(switch5 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));
				drawList->AddText(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 225.0f), ImColor(255.0f, 255.0f, 0.0f, 255.0f),
					"SW5");

				//lines

				//Sw1 to And2 
				drawList->AddLine(ImVec2(bb.Min.x + 45.0f, bb.Min.y + 15.0f), ImVec2(bb.Min.x + 140.0f, bb.Min.y + 30.0f),
					(switch1 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));
				//sw1 to xor1
				drawList->AddLine(ImVec2(bb.Min.x + 45.0f, bb.Min.y + 15.0f), ImVec2(bb.Min.x + 140.0f, bb.Min.y + 95.0f),
					(switch1 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//sw2 to And2
				drawList->AddLine(ImVec2(bb.Min.x + 45.0f, bb.Min.y + 70.0f), ImVec2(bb.Min.x + 140.0f, bb.Min.y + 30.0f),
					(switch2 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//sw2 to  or1
				drawList->AddLine(ImVec2(bb.Min.x + 45.0f, bb.Min.y + 70.0f), ImVec2(bb.Min.x + 155.0f, bb.Min.y + 135.0f),
					(switch2 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//sw3 to xor1
				drawList->AddLine(ImVec2(bb.Min.x + 45.0f, bb.Min.y + 115.0f), ImVec2(bb.Min.x + 140.0f, bb.Min.y + 95.0f),
					(switch3 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//sw3 to or1
				drawList->AddLine(ImVec2(bb.Min.x + 45.0f, bb.Min.y + 115.0f), ImVec2(bb.Min.x + 140.0f, bb.Min.y + 165.0f),
					(switch3 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//sw4 to and1
				drawList->AddLine(ImVec2(bb.Min.x + 45.0f, bb.Min.y + 180.0f), ImVec2(bb.Min.x + 80.0f, bb.Min.y + 185.0f),
					(switch4 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));


				//sw5 to and1
				drawList->AddLine(ImVec2(bb.Min.x + 45.0f, bb.Min.y + 235.0f), ImVec2(bb.Min.x + 80.0f, bb.Min.y + 185.0f),
					(switch5 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//sw5 to xor2
				drawList->AddLine(ImVec2(bb.Min.x + 45.0f, bb.Min.y + 235.0f), ImVec2(bb.Min.x + 140.0f, bb.Min.y + 205.0f),
					(switch5 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//and1 to xor2
				drawList->AddLine(ImVec2(bb.Min.x + 110.0f, bb.Min.y + 185.0f), ImVec2(bb.Min.x + 140.0f, bb.Min.y + 205.0f),
					(and[1].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//and2 to and3
				drawList->AddLine(ImVec2(bb.Min.x + 170.0f, bb.Min.y + 30.0f), ImVec2(bb.Min.x + 210.0f, bb.Min.y + 60.0f),
					(and[0].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//xor1 to and3
				drawList->AddLine(ImVec2(bb.Min.x + 170.0f, bb.Min.y + 95.0f), ImVec2(bb.Min.x + 210.0f, bb.Min.y + 60.0f),
					(xor[0].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//or1 to and4
				drawList->AddLine(ImVec2(bb.Min.x + 170.0f, bb.Min.y + 165.0f), ImVec2(bb.Min.x + 210.0f, bb.Min.y + 195.0f),
					(or [0].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//xor2 to and4
				drawList->AddLine(ImVec2(bb.Min.x + 170.0f, bb.Min.y + 205.0f), ImVec2(bb.Min.x + 210.0f, bb.Min.y + 195.0f),
					(xor[1].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//and3 to and5

				drawList->AddLine(ImVec2(bb.Min.x + 240.0f, bb.Min.y + 60.0f), ImVec2(bb.Min.x + 270.0f, bb.Min.y + 130.0f),
					(and[2].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));
				//and4 to and5
				drawList->AddLine(ImVec2(bb.Min.x + 240.0f, bb.Min.y + 195.0f), ImVec2(bb.Min.x + 270.0f, bb.Min.y + 130.0f),
					(and[3].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//and5 to output
				drawList->AddLine(ImVec2(bb.Min.x + 300.0f, bb.Min.y + 130.0f), ImVec2(bb.Min.x + 345.0f, bb.Min.y + 130.0f),
					(and[4].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));


				//Puzzle Gates
				//and1
				drawList->AddRectFilled(ImVec2(bb.Min.x + 80.0f, bb.Min.y + 170.0f),
					ImVec2(bb.Min.x + 110.0f, bb.Min.y + 200.0f),
					(and[1].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));


				//And2
				drawList->AddRectFilled(ImVec2(bb.Min.x + 140.0f, bb.Min.y + 15.0f),
					ImVec2(bb.Min.x + 170.0f, bb.Min.y + 45.0f),
					(and[0].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));


				//Xor1
				ImVec2 points[4]{ ImVec2(bb.Min.x + 140.0f,bb.Min.y + 95.0f),ImVec2(bb.Min.x + 155.0f,bb.Min.y + 75.0f),
					ImVec2(bb.Min.x + 170.0f,bb.Min.y + 95.0f),ImVec2(bb.Min.x + 155.0f,bb.Min.y + 115.0f) };

				drawList->AddConvexPolyFilled(points, 4, (xor[0].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)), false);

				//or1
				drawList->AddTriangleFilled(ImVec2(bb.Min.x + 155.0f, bb.Min.y + 135.0f),
					ImVec2(bb.Min.x + 140.0f, bb.Min.y + 165.0f), ImVec2(bb.Min.x + 170.0f, bb.Min.y + 165.0f),
					(or [0].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//xor2
				ImVec2 points2[4]{ ImVec2(bb.Min.x + 140.0f,bb.Min.y + 205.0f),ImVec2(bb.Min.x + 155.0f,bb.Min.y + 185.0f),
					ImVec2(bb.Min.x + 170.0f,bb.Min.y + 205.0f),ImVec2(bb.Min.x + 155.0f,bb.Min.y + 225.0f) };

				drawList->AddConvexPolyFilled(points2, 4, (xor[1].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)), false);


				//and3
				drawList->AddRectFilled(ImVec2(bb.Min.x + 210.0f, bb.Min.y + 45.0f),
					ImVec2(bb.Min.x + 240.0f, bb.Min.y + 75.0f),
					(and[2].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//and4
				drawList->AddRectFilled(ImVec2(bb.Min.x + 210.0f, bb.Min.y + 180.0f),
					ImVec2(bb.Min.x + 240.0f, bb.Min.y + 210.0f),
					(and[3].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//and5
				drawList->AddRectFilled(ImVec2(bb.Min.x + 270.0f, bb.Min.y + 115.0f),
					ImVec2(bb.Min.x + 300.0f, bb.Min.y + 145.0f),
					(and[4].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//output1
				drawList->AddCircleFilled(ImVec2(bb.Min.x + 365.0f, bb.Min.y + 130.0f), 20.0f, (and[4].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)), 30);
				result = diode[0].finalOutput;


}


			void ImGuiPuzzleSystem::puzzle2(ImDrawList* drawList, ImRect bb)
			{
				std::vector<Switch> switches = puzzle->switches;
				std::vector<andGate> and = puzzle->and;
				std::vector<OrGate> or = puzzle-> or ;
				std::vector<XorGate> xor = puzzle-> xor;
				std::vector<Diode> diode = puzzle->diode;
				//switch 1
				drawList->AddRectFilled(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 5.0f),
					ImVec2(bb.Min.x + 45.0f, bb.Min.y + 25.0f),
					(switch1 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));
				drawList->AddText(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 5.0f), ImColor(255.0f, 255.0f, 0.0f, 255.0f),
					"SW1");
				//switch 2
				drawList->AddRectFilled(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 60.0f),
					ImVec2(bb.Min.x + 45.0f, bb.Min.y + 80.0f),
					(switch2 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));
				drawList->AddText(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 60.0f), ImColor(255.0f, 255.0f, 0.0f, 255.0f),
					"SW2");
				//switch 3
				drawList->AddRectFilled(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 115.0f),
					ImVec2(bb.Min.x + 45.0f, bb.Min.y + 135.0f),
					(switch3 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));
				drawList->AddText(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 115.0f), ImColor(255.0f, 255.0f, 0.0f, 255.0f),
					"SW3");
				////switch 4
				drawList->AddRectFilled(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 170.0f),
					ImVec2(bb.Min.x + 45.0f, bb.Min.y + 190.0f),
					(switch4 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));
				drawList->AddText(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 170.0f), ImColor(255.0f, 255.0f, 0.0f, 255.0f),
					"SW4");
				//switch 5
				drawList->AddRectFilled(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 225.0f),
					ImVec2(bb.Min.x + 45.0f, bb.Min.y + 245.0f),
					(switch5 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));
				drawList->AddText(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 225.0f), ImColor(255.0f, 255.0f, 0.0f, 255.0f),
					"SW5");



				//LINES

				//sw1 to or1
				drawList->AddLine(ImVec2(bb.Min.x + 45.0f, bb.Min.y + 15.0f), ImVec2(bb.Min.x + 150.0f, bb.Min.y + 45.0f),
					(switch1 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//sw1 to and2
				drawList->AddLine(ImVec2(bb.Min.x + 45.0f, bb.Min.y + 15.0f), ImVec2(bb.Min.x + 150.0f, bb.Min.y + 115.0f),
					(switch1 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//sw2 to and1
				drawList->AddLine(ImVec2(bb.Min.x + 45.0f, bb.Min.y + 70.0f), ImVec2(bb.Min.x + 70.0f, bb.Min.y + 95.0f),
					(switch2 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));


				//sw3 to and2
				drawList->AddLine(ImVec2(bb.Min.x + 45.0f, bb.Min.y + 125.0f), ImVec2(bb.Min.x + 150.0f, bb.Min.y + 115.0f),
					(switch3 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//sw4 to and1

				drawList->AddLine(ImVec2(bb.Min.x + 45.0f, bb.Min.y + 180.0f), ImVec2(bb.Min.x + 70.0f, bb.Min.y + 95.0f),
					(switch4 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//sw4 to xor1
				drawList->AddLine(ImVec2(bb.Min.x + 45.0f, bb.Min.y + 180.0f), ImVec2(bb.Min.x + 150.0f, bb.Min.y + 190.0f),
					(switch4 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));


				//sw5 to or1
				drawList->AddLine(ImVec2(bb.Min.x + 45.0f, bb.Min.y + 235.0f), ImVec2(bb.Min.x + 150.0f, bb.Min.y + 45.0f),
					(switch5 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));


				//sw5 to xor2
				drawList->AddLine(ImVec2(bb.Min.x + 45.0f, bb.Min.y + 235.0f), ImVec2(bb.Min.x + 230.0f, bb.Min.y + 210.0f),
					(switch5 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//and1 to xor1
				drawList->AddLine(ImVec2(bb.Min.x + 100.0f, bb.Min.y + 95.0f), ImVec2(bb.Min.x + 150.0f, bb.Min.y + 190.0f),
					(and[0].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));


				//or1 to and4
				drawList->AddLine(ImVec2(bb.Min.x + 190.0f, bb.Min.y + 45.0f), ImVec2(bb.Min.x + 290.0f, bb.Min.y + 55.0f),
					(or[0].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//and2 to and3
				drawList->AddLine(ImVec2(bb.Min.x + 180.0f, bb.Min.y + 115.0f), ImVec2(bb.Min.x + 230.0f, bb.Min.y + 130.0f),
					(and[1].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));


				//xo1 to and3
				drawList->AddLine(ImVec2(bb.Min.x + 180.0f, bb.Min.y + 190.0f), ImVec2(bb.Min.x + 230.0f, bb.Min.y + 130.0f),
					(xor[0].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//xor1 to xor2
				drawList->AddLine(ImVec2(bb.Min.x + 180.0f, bb.Min.y + 190.0f), ImVec2(bb.Min.x + 230.0f, bb.Min.y + 210.0f),
					(xor[0].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));


				//and3 to output2
				drawList->AddLine(ImVec2(bb.Min.x + 260.0f, bb.Min.y + 130.0f), ImVec2(bb.Min.x + 385.0f, bb.Min.y + 128.0f),
					(and[2].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));
				
				//xor2 to and4
				drawList->AddLine(ImVec2(bb.Min.x + 260.0f, bb.Min.y + 210.0f), ImVec2(bb.Min.x + 290.0f, bb.Min.y + 55.0f),
					(xor[1].output? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));
				//xor2 to output3
				drawList->AddLine(ImVec2(bb.Min.x + 260.0f, bb.Min.y + 210.0f), ImVec2(bb.Min.x + 385.0f, bb.Min.y + 210.0f),
					(xor[1].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//and4 to output1
				drawList->AddLine(ImVec2(bb.Min.x + 320.0f, bb.Min.y + 55.0f), ImVec2(bb.Min.x + 385.0f, bb.Min.y + 55.0f),
					(and[3].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));



				//GATES

				//And1
				drawList->AddRectFilled(ImVec2(bb.Min.x + 70.0f, bb.Min.y + 80.0f),
					ImVec2(bb.Min.x + 100.0f, bb.Min.y + 110.0f),
					(and[0].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//or1
				drawList->AddTriangleFilled(ImVec2(bb.Min.x + 170.0f, bb.Min.y + 15.0f),
					ImVec2(bb.Min.x +  150.0f, bb.Min.y + 45.0f), ImVec2(bb.Min.x + 190.0f, bb.Min.y + 45.0f),
					(or[0].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//and2
				drawList->AddRectFilled(ImVec2(bb.Min.x + 150.0f, bb.Min.y + 100.0f),
					ImVec2(bb.Min.x + 180.0f, bb.Min.y + 130.0f),
					(and[1].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//xor1
				ImVec2 points[4]{ ImVec2(bb.Min.x + 150.0f,bb.Min.y + 190.0f),ImVec2(bb.Min.x + 165.0f,bb.Min.y + 170.0f),
					ImVec2(bb.Min.x + 180.0f,bb.Min.y + 190.0f),ImVec2(bb.Min.x + 165.0f,bb.Min.y + 210.0f) };
				drawList->AddConvexPolyFilled(points, 4, (xor[0].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)), false);

				//and3
				drawList->AddRectFilled(ImVec2(bb.Min.x + 230.0f, bb.Min.y + 115.0f),
					ImVec2(bb.Min.x + 260.0f, bb.Min.y + 145.0f),
					(and[2].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//xor2
				ImVec2 points2[4]{ ImVec2(bb.Min.x + 230.0f,bb.Min.y + 210.0f),ImVec2(bb.Min.x + 245.0f,bb.Min.y + 190.0f),
					ImVec2(bb.Min.x + 260.0f,bb.Min.y + 210.0f),ImVec2(bb.Min.x + 245.0f,bb.Min.y + 230.0f) };
				drawList->AddConvexPolyFilled(points2, 4, (xor[1].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)), false);

				//and4
				drawList->AddRectFilled(ImVec2(bb.Min.x + 290.0f, bb.Min.y + 40.0f),
					ImVec2(bb.Min.x + 320.0f, bb.Min.y + 70.0f),
					(and[3].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//output1
				drawList->AddCircleFilled(ImVec2(bb.Min.x + 385.0f, bb.Min.y + 55.0f), 20.0f, (and[3].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)), 30);

				//output2
				drawList->AddCircleFilled(ImVec2(bb.Min.x + 385.0f, bb.Min.y + 128.0f), 20.0f, (and[2].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)), 30);

				//output3
				drawList->AddCircleFilled(ImVec2(bb.Min.x + 385.0f, bb.Min.y + 210.0f), 20.0f, (xor[1].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)), 30);

				result = diode[0].finalOutput&&diode[1].finalOutput &&diode[2].finalOutput;
			}

			void ImGuiPuzzleSystem::puzzle3(ImDrawList* drawList, ImRect bb)
			{

				std::vector<Switch> switches = puzzle->switches;
				std::vector<andGate> and = puzzle->and;
				std::vector<OrGate> or = puzzle-> or ;
				std::vector<XorGate> xor = puzzle-> xor;
				std::vector<Diode> diode = puzzle->diode;
				//switch 1
				drawList->AddRectFilled(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 5.0f),
					ImVec2(bb.Min.x + 45.0f, bb.Min.y + 25.0f),
					(switch1 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));
				drawList->AddText(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 5.0f), ImColor(255.0f, 255.0f, 0.0f, 255.0f),
					"SW1");
				//switch 2
				drawList->AddRectFilled(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 60.0f),
					ImVec2(bb.Min.x + 45.0f, bb.Min.y + 80.0f),
					(switch2 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));
				drawList->AddText(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 60.0f), ImColor(255.0f, 255.0f, 0.0f, 255.0f),
					"SW2");
				//switch 3
				drawList->AddRectFilled(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 115.0f),
					ImVec2(bb.Min.x + 45.0f, bb.Min.y + 135.0f),
					(switch3 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));
				drawList->AddText(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 115.0f), ImColor(255.0f, 255.0f, 0.0f, 255.0f),
					"SW3");
				////switch 4
				drawList->AddRectFilled(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 170.0f),
					ImVec2(bb.Min.x + 45.0f, bb.Min.y + 190.0f),
					(switch4 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));
				drawList->AddText(ImVec2(bb.Min.x + 5.0f, bb.Min.y + 170.0f), ImColor(255.0f, 255.0f, 0.0f, 255.0f),
					"SW4");


				//LINES

				//sw1 to xor1
				drawList->AddLine(ImVec2(bb.Min.x + 45.0f, bb.Min.y + 15.0f), ImVec2(bb.Min.x + 140.0f, bb.Min.y + 20.0f),
					(switch1 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));
				//sw2 to and1
				drawList->AddLine(ImVec2(bb.Min.x + 45.0f, bb.Min.y + 70.0f), ImVec2(bb.Min.x + 70.0f, bb.Min.y + 95.0f),
					(switch2 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//sw3 to and2
				drawList->AddLine(ImVec2(bb.Min.x + 45.0f, bb.Min.y + 125.0f), ImVec2(bb.Min.x + 70.0f, bb.Min.y + 95.0f),
					(switch3 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//sw4 to or1
				drawList->AddLine(ImVec2(bb.Min.x + 45.0f, bb.Min.y + 180.0f), ImVec2(bb.Min.x + 180.0f, bb.Min.y + 135.0f),
					(switch4 ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//and1 to xor1
				drawList->AddLine(ImVec2(bb.Min.x + 100.0f, bb.Min.y + 95.0f), ImVec2(bb.Min.x + 140.0f, bb.Min.y + 20.0f),
					(and[0].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));
				//and1 to or1
				drawList->AddLine(ImVec2(bb.Min.x + 100.0f, bb.Min.y + 95.0f), ImVec2(bb.Min.x + 180.0f, bb.Min.y + 135.0f),
					(and[0].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));
				//xor1 to and2
				drawList->AddLine(ImVec2(bb.Min.x + 170.0f, bb.Min.y + 20.0f), ImVec2(bb.Min.x + 260.0f, bb.Min.y + 55.0f),
					(xor[0].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));
				//or1 to and2
				drawList->AddLine(ImVec2(bb.Min.x + 220.0f, bb.Min.y + 135.0f), ImVec2(bb.Min.x + 260.0f, bb.Min.y + 55.0f),
					(or[0].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//and2 to output
				drawList->AddLine(ImVec2(bb.Min.x + 290.0f, bb.Min.y + 55.0f), ImVec2(bb.Min.x + 385.0f, bb.Min.y + 55.0f),
					(and[1].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));
				//GATES

				//And1
				drawList->AddRectFilled(ImVec2(bb.Min.x + 70.0f, bb.Min.y + 80.0f),
					ImVec2(bb.Min.x + 100.0f, bb.Min.y + 110.0f),
					(and[0].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));


				//xor1
				ImVec2 points[4]{ ImVec2(bb.Min.x + 140.0f,bb.Min.y + 20.0f),ImVec2(bb.Min.x + 155.0f,bb.Min.y + 0.0f),
					ImVec2(bb.Min.x + 170.0f,bb.Min.y + 20.0f),ImVec2(bb.Min.x + 155.0f,bb.Min.y + 40.0f) };
				drawList->AddConvexPolyFilled(points, 4, (xor[0].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)), false);

				//or1
				drawList->AddTriangleFilled(ImVec2(bb.Min.x + 200.0f, bb.Min.y + 100.0f),
					ImVec2(bb.Min.x + 180.0f, bb.Min.y + 135.0f), ImVec2(bb.Min.x + 220.0f, bb.Min.y + 135.0f),
					(or [0].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//And2
				drawList->AddRectFilled(ImVec2(bb.Min.x + 260.0f, bb.Min.y + 40.0f),
					ImVec2(bb.Min.x + 290.0f, bb.Min.y + 70.0f),
					(and[1].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)));

				//output1
				drawList->AddCircleFilled(ImVec2(bb.Min.x + 385.0f, bb.Min.y + 55.0f), 20.0f, (and[1].output ? ImColor(0.0f, 255.0f, 0.0f, 255.0f) : ImColor(255.0f, 0.0f, 0.0f, 255.0f)), 30);
				result = diode[0].finalOutput;
			}

			void ImGuiPuzzleSystem::PostRender()
			{
				ICamera* cam = Scene::GetActiveCamera();
				CameraController *controller = cam->GetParent()->GetComponent<CameraController>();

				//ImGui::NewFrame();

				if (openPuzzle) {
				
					switch (puzzleNum)
					{
					case 1:

						puzzle->circuit1(switch1, switch2, switch3, switch4, switch5);
						break;

					case 2:
						puzzle->circuit2(switch1, switch2, switch3, switch4, switch5);
						break;

					case 3:
						puzzle->circuit5(switch1, switch2, switch3, switch4);
						break;
					}
					//ImGui::SetWindowPos(ImVec2(200, 800));
				
		
					if (ImGui::Begin("Puzzle", nullptr, ImGuiWindowFlags_AlwaysAutoResize)) {

						
						
						ImGui::Text("Choose the correct switches to fix the system:");


						ImGui::Checkbox("Switch 1:", &switch1);
						ImGui::Checkbox("Switch 2:", &switch2);
						ImGui::Checkbox("Switch 3:", &switch3);
						ImGui::Checkbox("Switch 4:", &switch4);
						if (puzzleNum!=3)
						{
							ImGui::Checkbox("Switch 5:", &switch5);
						}
						
						ImGui::Separator();

						ImGui::BeginChildFrame((ImGuiID)"Foo", ImVec2(450, 300));
						
						ImGuiWindow* window = ImGui::GetCurrentWindow();
						ImRect bb(window->DC.CursorPos, window->DC.CursorPos + ImVec2(400, 200));
						ImGui::ItemSize(bb);
						if (!ImGui::ItemAdd(bb, NULL))
						{
							
						}
						ImDrawList* drawList = ImGui::GetWindowDrawList();
						switch (puzzleNum)
						{
						case 1:
							puzzle1(drawList,bb);
							break;

						case 2:
							puzzle2(drawList, bb);
							break;
						case 3:
							puzzle3(drawList, bb);
							break;
						}
						
				
	
					    ImGui::EndChildFrame();
						ImGui::End();

						
					}
				}

				
			}
		}
	}
}