#include "Questbook.h"
#include "../Headers/game_logic/Scene.h"
#include <string>
#include <iostream>
#include <map>



using namespace clueless;
using namespace game_logic;
using namespace systems;

 std::vector<Quest*>* Questbook:: MyGameQuests;
std::vector<Quest*>*Questbook::MyActiveQuests;
std::vector<Quest*>*Questbook::MyCompleteQuests;
//ImGuiQuestSystem Questbook::myQuests2;
 bool Questbook::open=true;


struct EventTypesMap : public std::map<std::string, EventTypes>
{
	EventTypesMap()
	{
		this->operator[]("inventory-pickup") = InventoryPickup;
		this->operator[]("collider-empty") = ColliderEmpty;
		this->operator[]("collider-enter") = ColliderEnter;
		this->operator[]("puzzle") = Puzzle;
	};
	~EventTypesMap() {}
};


std::vector<Quest*>* Questbook::__LoadQuests(const char* file)
{

	//TO BE IMPLEMENTED WHEN XML DESIGN
	tinyxml2::XMLDocument myQuests;
	std::vector<Quest*>* quests=new std::vector<Quest*>();

	tinyxml2::XMLError error= myQuests.LoadFile(file);
	//checks if file is loaded
	if (error==tinyxml2::XML_SUCCESS)
	{
		tinyxml2::XMLElement* quest = myQuests.FirstChildElement("main")->FirstChildElement("quest");
		//if quest is existant
		while (quest)
		{
			Quest* tempQuest = new Quest();
			//quest id
			const char* questId = quest->Attribute("id");
			if (questId)
			{
				tempQuest->SetId(questId);
			}
			const char* name = quest->Attribute("name");
			if (name)
			{
				tempQuest->setTitle(name);
			}
			const char* description = quest->FirstChildElement("description")->GetText();
			if (description)
			{
				tempQuest->setDetails(description);
			}
			const char* preReq = quest->Attribute("preReqQuest");
			if (preReq)
			{
				tempQuest->addPreReq(preReq);
			}
			else
			{
				tempQuest->ActivateQuest();
				tempQuest->MakeVisible();
			}

			tinyxml2::XMLElement* task = quest->FirstChildElement("task");
			while (task)
			{
				Task* tempTask = new Task();
				const char* taskID = task->Attribute("id");
				if (taskID)
				{
					tempTask->SetTaskId(taskID);
				}
				const char* taskDescription = task->FirstChildElement("description")->GetText();
				if (taskDescription)
				{
					tempTask->SetTaskDetails(taskDescription);
				}
				const char* trigger = task->FirstChildElement("trigger")->Attribute("type");
				if (trigger)
				{
					static EventTypesMap events;
					EventTypes type = events[trigger];
					tempTask->setMyEventType(type);

				}
				if (trigger&&(strcmp(trigger, "collider-empty") == 0 || strcmp(trigger, "collider-enter") == 0))
				{
					//event types

					const char* triggerID = task->FirstChildElement("trigger")->FirstChildElement("trigger-collider")->Attribute("id");
					if (triggerID)
					{

					}
					const char* triggerMask = task->FirstChildElement("trigger")->FirstChildElement("trigger-collider-mask")->Attribute("id");
					if (triggerMask)
					{
						if (std::strcmp("player",triggerMask)==0)
						{
							tempTask->setFilterMask(COL_PLAYER);
						}
						if (std::strcmp("tool", triggerMask) == 0)
						{
							tempTask->setFilterMask(COL_TOOL);
						}

						if (std::strcmp("tool-use", triggerMask) == 0)
						{
							tempTask->setFilterMask(COL_TOOL_USE);
						}
						if (std::strcmp("debris", triggerMask) == 0)
						{
							tempTask->setFilterMask(COL_DEBRIS);
						}
						
					}
				}
				if (strcmp(trigger, "inventory-pickup") == 0)
				{
					const char* triggerItem = task->FirstChildElement("trigger")->FirstChildElement("trigger-item")->Attribute("id");
					if (triggerItem)
					{

					}
				}

				if (strcmp(trigger, "puzzle") == 0)
				{
					const char* triggerItem = task->FirstChildElement("trigger")->FirstChildElement("trigger-puzzle")->Attribute("id");
					if (triggerItem)
					{
						tempTask->setId(triggerItem);
					}
				}

				tempQuest->AddTask(tempTask);
				task = task->NextSiblingElement("task");

			}

			quests->push_back(tempQuest);
			quest = quest->NextSiblingElement("quest");
		}
	}
	

	return quests;
}



void Questbook::Init(const char* questFile)
{
	MyGameQuests= __LoadQuests(questFile);
	MyActiveQuests = new std::vector<Quest*>();
	MyCompleteQuests = new std::vector<Quest*>();
}
void Questbook::Update(Event &message)
{
	
	for (int i = 0; i <MyGameQuests->size(); i++)
	{
		if (MyGameQuests->at(i)->isActive())
		{
			MyActiveQuests->push_back(MyGameQuests->at(i));
			MyGameQuests->erase(MyGameQuests->begin() + i);
		}


	}
	for (int i = 0; i < MyActiveQuests->size(); i++)
	{
		MyActiveQuests->at(i)->HandleEvent(message);
		MyActiveQuests->at(i)->Update();

		if (MyActiveQuests->at(i)->IsCompleted())
		{

			MyCompleteQuests->push_back(MyActiveQuests->at(i));
			MyActiveQuests->erase(MyActiveQuests->begin() + i);

		}
		

	}

	message.type = NONE;
	message.data =nullptr;

	for (int i = 0; i < MyGameQuests->size(); i++)
	{
		std::string questId = MyGameQuests->at(i)->getId();
		std::vector<std::string> preReq = MyGameQuests->at(i)->getRequirements();
		int complete = 0;
		int size = preReq.size();
		for (int j = 0; j < MyCompleteQuests->size(); j++)
		{
			std::string completeId = MyCompleteQuests->at(j)->getId();
			for (int k = 0; k < size; k++)
			{
				std::string pre = preReq.at(k);
				if (pre==completeId)
				{
					complete += 1;
					break;
				}
			}
			if (complete==size)
			{
				MyGameQuests->at(i)->ActivateQuest();
				break;
			}

		}
	}
}
void Questbook::Draw()
{
	//myQuests.PostRender();
	if (MyActiveQuests->size()==0)
	{
		ImGuiQuestSystem::quest = "Demo Complete";
		ImGuiQuestSystem::questDetails = "You have completd the demo congrats";
		ImGuiQuestSystem::task = "Quit the game noob";
	}
	for (int i = 0; i < MyActiveQuests->size(); i++)
	{
		if (MyActiveQuests->at(i)->isVisible())
		{
			//std::cout << i + 1 << ".";
			std::string title = MyActiveQuests->at(i)->getTitle();
			std::string details = MyActiveQuests->at(i)->getDetails();
			std::vector<Task*> myTasks = MyActiveQuests->at(i)->getTasks();
			std::string taskDetails = "";
			ImGuiQuestSystem::quest = title.c_str();
			ImGuiQuestSystem::questDetails = details.c_str();
			for (int i = 0; i < myTasks.size(); i++)
			{

				if (!myTasks[i]->isComplete())
				{
					taskDetails += std::to_string(i + 1) + " " + myTasks[i]->getDetails() + "\n";
				}

			}
			ImGuiQuestSystem::task = taskDetails;
		}

	}
}
void Questbook::Unload()
{
	MyGameQuests->empty();
	MyActiveQuests->empty();
	MyCompleteQuests->empty();
}
