#include "ui/TrueTypeFont.h"
#include "utils/common.h"
#include "utils/Logger.h"

#include <memory>

namespace clueless { namespace ui {
	using namespace clueless::graphics;

	TrueTypeFont::TrueTypeFont(const char* fileName, uint32_t fontSize = 32) {
		myFontSize = fontSize;

		myFontData = readFile<unsigned char>(fileName);
		uint8_t* atlasData = new uint8_t[ATLAS_WIDTH *ATLAS_HEIGHT];
		
		myCharInfo = new stbtt_packedchar[CHAR_COUNT];

		if (!stbtt_InitFont(&myFontInfo, myFontData, 0)) {
			FILE_LOG(logERROR) << "Failed to initialize font";
			delete atlasData;
			return;
		}

		// Gets the font metrics
		stbtt_GetFontVMetrics(&myFontInfo, &myAscent, &myDescent, &myLineGap);

		myPixelHeightScale = stbtt_ScaleForPixelHeight(&myFontInfo, fontSize);
		myEmToPixel = stbtt_ScaleForMappingEmToPixels(&myFontInfo, 1);

		stbtt_pack_context context;
		if (!stbtt_PackBegin(&context, atlasData, ATLAS_WIDTH, ATLAS_HEIGHT, 0, 1, nullptr)) {
			FILE_LOG(logERROR) << "Failed to pack font texture";
			delete atlasData;
			return;
		}

		stbtt_PackSetOversampling(&context, FONT_OVERSAMPLE_X, FONT_OVERSAMPLE_Y);
		if (!stbtt_PackFontRange(&context, myFontData, 0, fontSize, FIRST_CHAR, CHAR_COUNT, myCharInfo)) {
			FILE_LOG(logERROR) << "Failed to pack font range";
			delete atlasData;
			return;
		}
		stbtt_PackEnd(&context);

		for (uint32_t ix = FIRST_CHAR; ix < FIRST_CHAR + CHAR_COUNT; ix++) {
			// TODO: Grab glyph data
		}

		myTexture = new Texture(ATLAS_WIDTH, ATLAS_HEIGHT, atlasData, GL_NEAREST, GL_ALPHA, GL_ALPHA, GL_UNSIGNED_BYTE);

		delete atlasData;
	}

	GlyphInfo TrueTypeFont::GetGlyph(int codepoint, float offsetX, float offsetY) const {
		stbtt_aligned_quad quad;

		stbtt_GetPackedQuad(myCharInfo, myTexture->GetWidth(), myTexture->GetHeight(), codepoint - FIRST_CHAR, &offsetX, &offsetY, &quad, 1);
		auto xmin = quad.x0;
		auto xmax = quad.x1;
		auto ymin = -quad.y1;
		auto ymax = -quad.y0;

		GlyphInfo info = GlyphInfo();
		info.OffsetX = offsetX;
		info.OffsetY = offsetY;
		info.Positions[0] = { xmax, -ymin, 0 };
		info.Positions[1] = { xmax, -ymax, 0 };
		info.Positions[2] = { xmin, -ymax, 0 };
		info.Positions[3] = { xmin, -ymin, 0 };
		info.UVs[0] = { quad.s1, quad.t1 };
		info.UVs[1] = { quad.s1, quad.t0 };
		info.UVs[2] = { quad.s0, quad.t0 };
		info.UVs[3] = { quad.s0, quad.t1 };

		return info;
	}

	int TrueTypeFont::GetKerning(int char1, int char2) const {
		return stbtt_GetCodepointKernAdvance(&myFontInfo, char1, char2) * myPixelHeightScale;
	}

	int TrueTypeFont::GetLineHeight() const {
		return (myAscent - myDescent + myLineGap) * myPixelHeightScale;
	}

	void TrueTypeFont::Unload() {

	}

	TrueTypeFont::~TrueTypeFont() {
		delete myTexture;
		delete myCharInfo;
		delete myFontData;
	}
} }