#include "ui/ImGuiTools.h"

#include "ui/input/InputManager.h"
#include "game_logic/Scene.h"

#include <imgui/imgui.h>
#include <imgui/imgui_impl.h>

using namespace clueless::ui;
using namespace clueless::ui::input;
using namespace clueless::game_logic;

namespace clueless {

	void ImGuiTools::Init() {
		imgui_impl::ImGuiInit();
		InputManager::AddTextEventCallback([](void*, uint32_t codePoint, int mods) {
			ImGui::GetIO().AddInputCharacter(codePoint);
		});
		InputManager::AddKeyEventCallback([](void*, int key, int scanCode, ButtonState state, int mods) {
			ImGui::GetIO().KeysDown[key] = (state != ButtonState::ButtonReleased);
		});
		InputManager::AddMouseScrollCallback([](void*, double xScroll, double yScroll) {
			ImGui::GetIO().MouseWheel = yScroll;
		});
		InputManager::AddMouseButtonEventCallback([](void*, int button, ButtonState state, int mods) {

			switch (button) {
			case GLFW_MOUSE_BUTTON_LEFT: // Left mouse
				ImGui::GetIO().MouseDown[0] = state != ButtonReleased;
				break;
			case GLFW_MOUSE_BUTTON_MIDDLE: // Middle mouse
				ImGui::GetIO().MouseDown[2] = state != ButtonReleased;
				break;
			case GLFW_MOUSE_BUTTON_RIGHT: // Right mouse
				ImGui::GetIO().MouseDown[1] = state != ButtonReleased;
				break;
			default:
				break;

			}
		});
		InputManager::AddMouseMoveCallback([](void*, glm::vec2 pos, glm::vec2 delta) {
			ImGui::GetIO().MousePos = pos;
		});
		graphics::Window* window = Scene::GetWindow();
		window->AddResizeCallback([](void*, int width, int height) {
			ImGuiIO &io = ImGui::GetIO();
			io.DisplaySize.x = width;
			io.DisplaySize.y = height;
		});
	}

	void ImGuiTools::NewFrame() {
		ImGui::NewFrame();
	}

	void ImGuiTools::RenderFrame() {
		ImGui::Render();
	}
}