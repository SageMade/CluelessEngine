#ifndef STACK_CPP
#define STACK_CPP

#include <stdexcept>

#include <utils/Stack.h>
#include <utils/common.h>

namespace clueless {

    template <typename T>
    Stack<T>::Stack(int capacity) {
        if (capacity <= 0)
            throw std::invalid_argument("Capacity must be > 0");

        myCapacity = capacity;
        myTopIndex = -1;
        myArray = new T[capacity];
    }

    template <typename T>
    Stack<T>::~Stack() {
        delete[] myArray;
    }

    template <typename T>
    void Stack<T>::Push(const T& value) {
        if (myTopIndex >= myCapacity - 1)
            throw std::out_of_range("Stack has reached capacity");

        myTopIndex ++;
        myArray[myTopIndex] = value;
    }

    template <typename T>
    T Stack<T>::Pop() {
        if (IsEmpty())
            throw EmptyStackError();

        T result = myArray[myTopIndex];
        myTopIndex --;
        return result;
    }

    template <typename T>
    void Stack<T>::Resize(const int newSize) {
        if (newSize <= myTopIndex)
            throw std::invalid_argument("Cannot resize stack to be smaller than it's current contents");

        T* newArray = new T[newSize];


        for(int ix = 0; ix <= myTopIndex; ix ++)
            newArray[ix] = myArray[ix];

        delete[] myArray;
        myArray = newArray;
        myCapacity = newSize;
    }

    template <typename T>
    T Stack<T>::Peek() const {
        if (myTopIndex == -1)
            throw std::out_of_range("Stack has no elements");

        return myArray[myTopIndex];
    }

    template <typename T>
    T Stack<T>::At(const int index) const {
        if (index < 0 || index > myTopIndex)
            throw index_error();

        return myArray[index];
    }

    template <typename T>
    int Stack<T>::Size() const {
        return myTopIndex + 1;
    }

    template <typename T>
    bool Stack<T>::IsFull() const {
        return myTopIndex >= myCapacity -1;
    }

    template <typename T>
    bool Stack<T>::IsEmpty() const {
        return myTopIndex == -1;
    }
}

#endif
