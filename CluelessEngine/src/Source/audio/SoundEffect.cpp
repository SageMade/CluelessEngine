#include "audio/SoundEffect.h"
#include "audio/SoundManager.h"
#include "utils/common.h"

namespace clueless { namespace audio {

    SoundEffect::SoundEffect(const std::string name, const std::string fileName)
    {
        myName = name;
        myFilename = fileName;

        mySoundHandle = gau_load_sound_file(fileName.c_str(), getFileExtension(fileName.c_str()));

        myGain  = 1.0f;
        myPan   = 0.0f;
        myPitch = 1.0f;
    }
	
	void SoundEffect::Unload() {
		if (mySoundHandle != nullptr) {
			ga_sound_release(mySoundHandle);
			mySoundHandle = nullptr;
		}
	}

    SoundInstance* SoundEffect::MakeInstance() const {
        SoundInstance* instance = new SoundInstance(this, myGain, myPan, myPitch);
        return instance;
    }

    void SoundEffect::SetGain(const float value) {
        myGain = value;
    }

    void SoundEffect::SetPitch(const float value) {
        myPitch = value;
    }

    void SoundEffect::SetPan(const float value) {
        myPan = value;
    }

} }
