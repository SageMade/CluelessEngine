#include "audio/SoundInstance.h"
#include "audio/SoundManager.h"

namespace clueless { namespace audio {



    void setFlagAndDestroyOnFinish(ga_Handle* handle, void* context) {
        gc_int32* flag = (gc_int32*)context;
        *flag = 1;
        ga_handle_destroy(handle);
    }

    void loopOnFinish(ga_Handle* handle, void* context) {
        SoundInstance* instance = (SoundInstance*)handle->userContext;
		ga_handle_play(handle);
    }

    SoundInstance::SoundInstance(const SoundEffect* sound, const float gain, const float pan, const float pitch)
    {
        mySound = sound;
        myGain  = gain;
        myPan   = pan;
        myPitch = pitch;

        myHandle     = nullptr;
		myLoopSrc    = nullptr;
        mySeekOffset = 0;
    }

	SoundInstance::~SoundInstance() {
		if (myHandle)
			ga_handle_destroy(myHandle);
	}

	SoundInstance * SoundInstance::SetEffect(const SoundEffect * sound){
		mySound = sound;
		return this;
	}

	SoundInstance* SoundInstance::SetGain(const float value) {
        myGain = value;
        if (ga_handle_playing(myHandle)) {
            ga_handle_setParamf(myHandle, GA_HANDLE_PARAM_GAIN, value);
        }
        return this;
    }

    SoundInstance* SoundInstance::SetPitch(const float value) {
        myPitch = value;
        if (ga_handle_playing(myHandle)) {
            ga_handle_setParamf(myHandle, GA_HANDLE_PARAM_PITCH, value);
        }
        return this;
    }

    SoundInstance* SoundInstance::SetPan(const float value) {
        myPan = value;
        if (ga_handle_playing(myHandle)) {
            ga_handle_setParamf(myHandle, GA_HANDLE_PARAM_PAN, value);
        }
        return this;
    }

    SoundInstance* SoundInstance::Play() {
        gc_int32 quit = 0;
		//if (myHandle != NULL)
		//	ga_handle_destroy(myHandle);
        myHandle = gau_create_handle_sound(SoundManager::__myMixer, mySound->mySoundHandle, (ga_FinishCallback)&setFlagAndDestroyOnFinish, &quit, NULL);
        myHandle->userContext = this;
        ga_handle_play(myHandle);
        ga_handle_setParamf(myHandle, GA_HANDLE_PARAM_PAN, myPan);
        ga_handle_setParamf(myHandle, GA_HANDLE_PARAM_PITCH, myPitch);
        ga_handle_setParamf(myHandle, GA_HANDLE_PARAM_GAIN, myGain);
        ga_handle_seek(myHandle, mySeekOffset);
        return this;
    }

    SoundInstance* SoundInstance::Loop() {
		//if (myHandle != NULL)
		//	ga_handle_destroy(myHandle);
        myHandle = gau_create_handle_sound(SoundManager::__myMixer, mySound->mySoundHandle, 0, 0, &myLoopSrc);
        gau_sample_source_loop_set(myLoopSrc, -1, 0);
        myHandle->userContext = this;
        ga_handle_play(myHandle);
        ga_handle_setParamf(myHandle, GA_HANDLE_PARAM_PAN, myPan);
        ga_handle_setParamf(myHandle, GA_HANDLE_PARAM_PITCH, myPitch);
        ga_handle_setParamf(myHandle, GA_HANDLE_PARAM_GAIN, myGain);
        ga_handle_seek(myHandle, mySeekOffset);
        return this;
    }

    SoundInstance* SoundInstance::Stop() {
		if (myLoopSrc)
			gau_sample_source_loop_clear(myLoopSrc);
        ga_handle_stop(myHandle);
        mySeekOffset = 0;
        return this;
    }

    SoundInstance* SoundInstance::Pause() {
        if (IsPlaying()) {
            mySeekOffset = ga_handle_tell(myHandle, GA_TELL_PARAM_CURRENT);
            ga_handle_stop(myHandle);
        }
        return this;
    }

    bool SoundInstance::IsPlaying() const {
        if (myHandle == NULL)
            return false;
        return ga_handle_playing(myHandle);
    }

} }
