#include "audio/SoundManager.h"
#include "utils/Logger.h"

namespace clueless { namespace audio {


    gau_Manager *SoundManager::__myManager = nullptr;
    ga_Mixer    *SoundManager::__myMixer   = nullptr;

    std::vector<SoundEffect*> SoundManager::__mySounds;

	// Initialize the audio manager
    void SoundManager::Init() {
		// Lot's of logging! Also, basically just set up Gorilla Audio
        FILE_LOG(logDEBUG) << "Initializing audio subsystem...";

        FILE_LOG(logDEBUG) << "\tInit GC";
        gc_initialize(0);
        FILE_LOG(logDEBUG) << "\tInit Audio Manager";
        __myManager = gau_manager_create_custom(GA_DEVICE_TYPE_DEFAULT, GAU_THREAD_POLICY_MULTI, 4, 512);
        FILE_LOG(logDEBUG) << "\tInit Audio Mixer";
        __myMixer = gau_manager_mixer(__myManager);

        FILE_LOG(logDEBUG) << "Done";
    }

	// Adds a new sound to the effect pool
    void SoundManager::Add(SoundEffect* sound) {
        __mySounds.push_back(sound);
    }

	// Gets the sound effect for the given name
    SoundEffect* SoundManager::Get(const std::string name) {
        for(SoundEffect* sound : __mySounds) {
            if (sound->GetName() == name)
                return sound;
        }
        return nullptr;
    }

	// Gets a sound effect instance for the given effect name
    SoundInstance* SoundManager::GetInstance(const std::string name) {
        for(SoundEffect* sound : __mySounds) {
            if (sound->GetName() == name)
                return sound->MakeInstance();
        }
        return nullptr;
    }

	// Update the audio manager
    void SoundManager::Update() {
		gau_manager_update(__myManager);
    }

	// Handles cleaning up the audio resources
    void SoundManager::Cleanup() {
		// Clean up the sounds
        for(int i = 0; i < __mySounds.size(); i ++)
            delete __mySounds[i];

		// Destroy the manager and shut down
        gau_manager_destroy(__myManager);
        gc_shutdown();
    }


} }
