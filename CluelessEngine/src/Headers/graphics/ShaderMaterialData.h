#pragma once

#include "glm_math.h"

namespace clueless { namespace graphics {

	struct ShaderMaterialData {
		glm::vec4 ObjectColor;
		float     TextureContribution;
		float     SpecularPower;
		uint32_t  TextureId;
	};

} }