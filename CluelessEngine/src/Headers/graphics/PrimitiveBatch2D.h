#pragma once

#include "glm_math.h"
#include <glm\gtx\matrix_transform_2d.hpp>

#include "VertexLayouts.h"
#include "utils/Stack.h"
#include "graphics/Shader.h"

#include "Colors.h"

#define RENDER2D_LINES_SIZE 1000
#define RENDER2D_TRIS_SIZE  15000

namespace clueless {
	namespace graphics {

		class PrimitiveBatch2D {
			public:
			static glm::mat4 ViewProj;

			static void Init();

			static void PushTransform(const glm::mat3& transform);
			static void PushOverride(const glm::mat3& transform);
			static void PopTransform();
			
			static void PushLine(const glm::vec2& pos1, const glm::vec2& pos2, const glm::vec4& color = WHITE);
			static void PushLine(const glm::vec2& pos1, const glm::vec2& pos2, const glm::vec4& fromColor, const glm::vec4& toColor);
			static void PushTriangle(const glm::vec2& pos1, const glm::vec2& pos2, const glm::vec2& pos3, const glm::vec4& color = WHITE);

			static void PushEllipse(const glm::vec2& center, const glm::vec2& size, const glm::vec4& color = WHITE, uint8_t segmentRatio = 1);

			static void Flush();

			private:
			struct PrimitiveVert2D {
				glm::vec2 Position;
				glm::vec4 Color;
				glm::vec2 TexUV;
				uint8_t   TexID;
				float     TexPower;
			};

			static glm::mat3       myCurrent;

			static PrimitiveVert2D *myTriVertices;
			static PrimitiveVert2D *myLineVertices;

			static uint32_t         myBufferCounts[2];
			static GLuint           myVertexBuffers[2];
			static GLuint           myVaos[2];

			static Shader          *myShader;
			static Stack<glm::mat3> myTransformStack;
		};

	}
}