#ifndef MATERIAL_H
#define MATERIAL_H

#include "Texture.h"
#include "Shader.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>

#include "cms/CMS.h"

#include "graphics/ShaderMaterialData.h"

namespace clueless { namespace graphics {
	
	enum TextureUsage {
		TextureDiffuse = 1,
		TextureSpecular = 2,
		TextureEmissive = 3,
		TextureNormal = 4,
		TextureUnknown = 5
	};

	struct MaterialTexture {
		Texture*     TextureValue;
		TextureUsage Usage;

		MaterialTexture() : TextureValue(nullptr), Usage(TextureUnknown) {}
		MaterialTexture(Texture* texture, TextureUsage usage) : TextureValue(texture), Usage(usage) {}
	};

    class Material
    {
        public:
            /** Default constructor */
            Material();
            /** Default destructor */
            virtual ~Material();

			void SetTexture(Texture* texture, TextureUsage usage);
			void Unload();

			void Apply();
			void SetWorldTransform(const glm::mat4& transform);
			void SetModelTransform(const glm::mat4& transform);
			void SetViewMatrix(const glm::mat4& value);
			void SetProjectionMatrix(const glm::mat4& value);

			void LoadIdentity();

			void SetShaderToken(ContentToken &token) { myShader = token; };
			ContentToken GetShaderToken() const { return myShader; }

			virtual void OnApply() {};

			static Material* FromAssimp(aiMaterial& material);

			static void InvalidateCurrent() { BoundMaterial = -1; }
			
        protected:
			Texture* myDiffuseTex;
			Texture* myNormalTex;
			Texture* myEmissiveTex;
			Texture* mySpecularTex;
			
			ContentToken myShader;

			float    myShininess;

			bool     isWireframe;
			bool     isDualSided;

			glm::vec3 myDiffuseColor;
			glm::vec3 mySpecularColor;
			glm::vec3 myAmbientColor;
			glm::vec3 myEmissiveColor;

			int myMaterialId;

			aiShadingMode mySuggestedShading;

			static Texture* __TryGetTexture(aiMaterial& material, aiTextureType type);

			static int MaterialCount;
			static int BoundMaterial;

        private:
    };
}}

#endif // MATERIAL_H
