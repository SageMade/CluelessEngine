#ifndef GBUFFER_H
#define GBUFFER_H

#include <graphics/common.h>

namespace clueless { namespace graphics {

    class GBuffer
    {
        public:
			GBuffer(const uint32_t width, const uint32_t height, const GLenum wrapMode = GL_CLAMP, const bool multiSample = false);  
			 
            void Cleanup();

            void Bind() const;
            static void Unbind();

            GLuint GetTextureHandle() const { return myRenderedTexture; }
			GLuint GetNormalTextureHandle() const { return myNormalTexture; }
			GLuint GetDepthTexture() const { return myDepthTexture; }
			GLuint GetEmissiveTexture() const { return myEmmisiveTexture; }
			GLuint GetSpecularTexture() const { return mySpecularTexture; }
			GLuint GetWorldSpaceTexture() const { return myWorldSpaceTexture; }

			uint32_t GetWidth() { return myWidth; }
			uint32_t GetHeight() { return myHeight; }

			uint32_t GetFboHandle() { return myFrameBufferHandle; }

        private:
            void     __Create(const uint32_t width, const uint32_t height, const GLenum wrapMode, const bool multisample = false);
			void     __CreateTexture(GLuint& texHandle, const uint32_t width, const uint32_t height, const GLenum wrapMode, const GLenum bindingLoc, 
						const GLenum internalFormat = GL_RGBA, const GLenum format = GL_RGBA, const GLenum type = GL_UNSIGNED_BYTE,
						const bool multisample = false);

            uint32_t myWidth, myHeight;
            GLuint   myFrameBufferHandle;
            GLuint   myRenderedTexture, myNormalTexture, myDepthTexture, myEmmisiveTexture, mySpecularTexture, myWorldSpaceTexture;
            GLuint   myDepthRenderBuffer;
    };

} }

#endif // GBUFFER_H
