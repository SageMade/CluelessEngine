#ifndef RENDERABLE2D_H
#define RENDERABLE2D_H

#include "glm_math.h"

namespace clueless { namespace graphics { namespace renderer {

    enum Renderable2DType {
        Quad,
        Polygon,
        Advanced
    };

    class Renderable2D
    {
        public:
            Renderable2D();
            virtual ~Renderable2D();

            virtual Renderable2DType GetType() = 0;

            const glm::vec2& GetPosition() const { return myPosition; };
            const glm::vec4& GetColor() const { return myColor; };

        protected:
            glm::vec2 myPosition;
            glm::vec4 myColor;

        private:
    };

} } }

#endif // RENDERABLE2D_H
