#pragma once
#ifndef RENDERER_2D_H
#define RENDERER_2D_H

#include "glm_math.h"

#include <graphics/renderer/Renderable2D.h>
#include <utils/Stack.h>

namespace clueless { namespace graphics { namespace renderer {

    class Renderer2D {
        public:
            Renderer2D();

            virtual void Begin() = 0;
            virtual void Submit(const Renderable2D* renderable) = 0;
            virtual void End()   = 0;
            virtual void Flush() = 0;

            void PushTransform(glm::mat4 value);
            glm::mat4 PopTransform();
            void ClearTransform();

        protected:
            Stack<glm::mat4> myTransformStack;
    };

} } }

#endif // RENDERER_2D_H
