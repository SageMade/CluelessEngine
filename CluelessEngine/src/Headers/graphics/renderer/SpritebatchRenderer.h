#ifndef SPRITEBATCHRENDERER_H
#define SPRITEBATCHRENDERER_H

#include <graphics/renderer/Renderer2D.h>

namespace clueless { namespace graphics { namespace renderer {

    class SpritebatchRenderer : public Renderer2D
    {
        public:
            /** Default constructor */
            SpritebatchRenderer();
            /** Default destructor */
            virtual ~SpritebatchRenderer();

            virtual void Begin() override;
            virtual void Submit(const Renderable2D* renderable) override;
            virtual void End() override;
            virtual void Flush() override;

        private:
            struct VertexPositionColorTexture;
            VertexPositionColorTexture* myData;
    };

} } }

#endif // SPRITEBATCHRENDERER_H
