#pragma once

#include <cstdint>
#include "glm_math.h"

#include "anim/PoseData.h"

namespace clueless { namespace anim {

	struct PoseProvider {
		virtual ~PoseProvider() {}

		float      Time;
		uint16_t   JointCount;
		virtual PoseData GetJointTransform(uint16_t jointId) const = 0;
	};
	
	struct PoseFrame :
		public PoseProvider
	{
		PoseData *PoseTransforms;

		PoseData GetJointTransform(uint16_t jointId) const;
	};

	struct PartialPoseFrame :
		public PoseProvider
	{
		PoseData  *PoseTransforms;
		uint16_t  *JointIds;

		PoseData GetJointTransform(uint16_t jointId) const;
	};

}}