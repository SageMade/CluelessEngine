#pragma once

#include <cstdint>

#include "utils/FileHelpers.h"

#include <fstream>

namespace clueless { namespace anim {
	
	struct ChannelKeyframe
	{
		float Time;
		void* Value;

		ChannelKeyframe() :
			Time(0),
			Value(nullptr)
		{}

		template <typename T>
		void Set(const T& value) {
			Value = malloc(sizeof(T));
			memcpy(Value, &value, sizeof(T));
		}

		template <typename T>
		void Set(T* value) {
			Value = value;
		}

		template <typename T>
		T& Get() {
			return *reinterpret_cast<T*>(Value);
		}
	};

	struct ChannelMeta
	{
		uint32_t DataOffset;
		uint16_t NameLength;
		char*    Name;
		uint8_t  KeyFrameType;
		uint16_t KeyFrameSize;
		uint16_t NumKeyframes;
		ChannelKeyframe* Data;

		ChannelMeta() :
			NumKeyframes(0),
			KeyFrameSize(0),
			KeyFrameType(0),
			DataOffset(0),
			NameLength(0),
			Name(nullptr),
			Data(nullptr)
		{}

		void WriteToFile(std::fstream& stream);

		void ReadFromFile(std::fstream& stream);
	};
	
	struct AnimationHeader
	{
		char HeaderCode[4];
		uint16_t MajorVersion;
		uint8_t MinorVersion;
		uint32_t DataOffset;
		float TotalLength;
		uint16_t NameLength;
		char *Name;
		uint16_t ChannelCount;
		ChannelMeta *ChannelMetas;

		AnimationHeader() :
			MajorVersion(0),
			MinorVersion(1),
			DataOffset(0),
			TotalLength(0.0f),
			NameLength(0),
			Name(nullptr)
		{
			HeaderCode[0] = 'A';
			HeaderCode[1] = 'N';
			HeaderCode[2] = 'I',
			HeaderCode[3] = 'M';
		}

		void WriteToFile(std::fstream& stream);

		void ReadFromFile(std::fstream& stream);
	};

} }