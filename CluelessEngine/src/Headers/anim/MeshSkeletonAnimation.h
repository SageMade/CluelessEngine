#pragma once

#include <cstdint>
#include <string>
#include "anim/PoseProviders.h"
#include "anim/MeshSkeletonPose.h"
#include "anim/Enums.h"

namespace clueless { namespace anim {
	
	struct MeshSkeletonAnimation :
		public PoseProvider
	{
		typedef PoseProvider* PoseProvider_ptr;

		std::string       Name;
		PoseProvider_ptr *Frames;
		uint16_t          FrameCount;

		MeshSkeletonAnimation();
		MeshSkeletonAnimation(PoseProvider_ptr *frames, int frameCount);
		~MeshSkeletonAnimation();

		void Update(float dt);
		
		PoseProvider* GetCurrentFrame() const;
		PoseProvider* GetNextFrame() const;

		PoseData GetJointTransform(uint16_t jointId) const;

	private:
		uint16_t myCurrentFrame;
		uint16_t myNextFrame;

		float    myTime;
		float    mySpeedMult;
	};

} }