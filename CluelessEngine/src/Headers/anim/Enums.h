#pragma once

namespace clueless { namespace anim {

	enum LoopType {
		LoopType_Stop    = 0,
		LoopType_Repeat  = 1,
		LoopType_Reverse = 2,
	};

} }