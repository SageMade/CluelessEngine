#pragma once

namespace clueless {
	class ImGuiTools {
	public:
		static void Init();
		static void NewFrame();
		static void RenderFrame();
	};
}