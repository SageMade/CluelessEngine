#pragma once

#include <cstdint>

struct GraphicsConfig {
	static uint16_t BackBufferWidth;
	static uint16_t BackBufferHeight;
	static uint16_t ScreenWidth;
	static uint16_t ScreenHeight;
};