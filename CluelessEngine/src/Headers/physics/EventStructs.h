#pragma once

#include <BulletCollision\CollisionDispatch\btCollisionObject.h>

#include "glm_math.h"

namespace clueless { namespace physics {

	enum PhysicsEventType {
		PhysicsEvent_Collide = 0x01,
		PhysicsEvent_Enter   = 0x02,
		PhysicsEvent_Leave   = 0x02,
	};

	struct PhysicsEvent {
		btCollisionObject *ColliderA;
		btCollisionObject *ColliderB;
	};

	struct PhysicsInteract {
		const btCollisionObject *Object;
		glm::vec3                HitPointWorld;
		glm::vec3                HitPointLocal;
		glm::vec3                HitNormal;
	};

} }