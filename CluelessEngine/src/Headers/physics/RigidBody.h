#pragma once

#include <cstdint>
#include "PhysicsWorld.h"
#include "Transform.h"

#include "glm_math.h"


namespace clueless { namespace physics {

	class RigidBody {
		public:
			friend class PhysicsWorld;
			
			RigidBody(btCollisionShape* shape, float mass = 0, bool ownsShape = true);
			RigidBody(btCollisionShape* shape, btVector3 initialPosition, float mass = 0, bool ownsShape = true);
			RigidBody(btCollisionShape* shape, btVector3 initialPosition,  btQuaternion rotation, float mass = 0, bool ownsShape = true);
			~RigidBody();

			void SetGroup(uint8_t group) { myCollisionGroup = group; }
			void SetMask(uint8_t mask){ myCollisionMask = mask; }

			glm::vec3 GetPosition() const;
			glm::quat GetOrientation();

			void SetOrientation(const glm::quat& value);

			void SetTransform(Transform &value);
			void SetTransform(glm::mat4 &value);
			Transform GetTransform();

			btRigidBody *GetInternalBody() const { return myRigidBody;  }

			uint8_t GetGroup() const { return myCollisionGroup; }
			uint8_t GetMask() const { return myCollisionMask; }

			void ApplyImpulse(const glm::vec3& impulse);
			

		private:
			bool              ownsShape;
			uint8_t           myCollisionGroup;
			uint8_t           myCollisionMask;
			Transform         myTransform;
			btCollisionShape *myShape;
			btMotionState    *myMotionState;
			btRigidBody      *myRigidBody;
			PhysicsWorld     *myWorld;
	};

} }