
#include <iostream>
#include <string>
#include <vector>
#include "Event.h"


enum taskImportance
{
	REQUIRED = 0,
	NOTREQUIRED = 1
};

enum taskType
{
	AREA,
	FETCH,
	KILL,
	REPAIR
};

class Task
{
	private:
		//task status
		bool myComplete = false;
		bool myActive = false;
		bool myVisible = false;

		//task details
		std::string id;
		std::string myTitle;
		std::string myDetails;
		std::string myCompletedDetails;

		std::string myId="";
		taskImportance myImportance;
		int FilterMask;
		EventTypes myTaskType;

		
	public:
		Task();
		Task(taskImportance ti,std::string title,std::string details, 
			std::string complete, EventTypes type);
		~Task();
		void CompleteTask();
		taskImportance CheckTaskImportance();
		void SetTaskId(std::string id);
		void SetTaskDetails(std::string details);
		std::string getDetails();

		void setId(std::string id);
		void setFilterMask(int mask);
		void setMyEventType(EventTypes type);
		std::string ViewTaskDetails();
		void SetTaskCompleteDetails(std::string complete);
		std::string GetCompleteDetails();
		bool isComplete();
		bool IsVisible();
		bool IsActive();
		void Draw();
		void Update();
		void HandleEvent(Event &message);


};