#pragma once
#ifndef EVENTS_H
#define EVENTS_H

#include <functional>
#include <map>
#include <vector>

namespace clueless {

#define MESSAGE_PHYSICS         0x0001
#define MESSAGE_UI              0x0002
#define EVENT_PHYSICS_INTERACT  0x0003
#define MESSSAGE_VOLUME_TRIGGER 0x0004

	enum MessageHandling {
		MessageHandle_None = 0,
		MessageHandle_DispatchChildren = 0x01,
		MessageHandle_DispatchUpwards  = 0x02,

		MessageHandle_Dispatch2Way     = 0x01 | 0x02
	};

	struct MessageEvent {
		uint32_t        TypeCode;
		MessageHandling HandleMode;

		MessageEvent(uint32_t typeCode) :
			TypeCode(typeCode), myMetaData(nullptr), HandleMode(MessageHandle_Dispatch2Way)  { }

		template<typename T>
		MessageEvent(uint32_t typeCode, const T& data) :
			TypeCode(typeCode), myMetaData(nullptr), HandleMode(MessageHandle_Dispatch2Way) { 
			SetMeta(data);
		}

		template<typename T>
		MessageEvent(uint32_t typeCode, const T& data, MessageHandling handleMode) :
			TypeCode(typeCode), myMetaData(nullptr), HandleMode(handleMode) {
			SetMeta(data);
		}

		template <typename T>
		void SetMeta(T* data) {
			myMetaSize = sizeof(T*);
			myMetaData = (void*)data;
		}

		template <typename T>
		void SetMeta(const T& data) {
			myMetaSize = sizeof(T);
			myMetaData = malloc(myMetaSize);
			memcpy(myMetaData, &data, myMetaSize);
		}

		template <typename T>
		T* GetMeta() const {
			return (T*)myMetaData;
		}

		private:
			void*    myMetaData;
			size_t   myMetaSize;
	};

    class Events
    {
        public:
            static void RegisterEventCallback(const char* name, std::function<void()> event);
			static void InvokeEvent(const char* name);

            static void DumpEventList();

        protected:

        private:
            static std::map<const char*, std::vector<std::function<void()>>> myEvents;
            static Events mySingleton;
    };

}

#endif // EVENTS_H
