#pragma once

#include "GameTime.h"

#include <cstdint>

#include "Events.h"

namespace clueless {
	namespace game_logic {

		class GameObject;

		class GameComponent {
		public:
			GameComponent() : myObject(nullptr) {};
			GameComponent(GameObject *myObject);
			virtual ~GameComponent();

			void SetParent(GameObject *parent) { myObject = parent; }

			virtual void OnEnabled() {};
			virtual void OnDisabled() {};
			virtual void OnStart() {};

			virtual void AddedToScene() {};
			virtual void RemovedFromScene() {};

			virtual void PrePhysics() {};

			virtual void Render() {}; 

			virtual void Dispatch(const MessageEvent& message);
			virtual void HandleMessage(const MessageEvent& message, GameComponent *dispatchedFrom = nullptr) {};

			virtual void Update() {};
			virtual void FixedUpdate() {};
			virtual void LateUpdate() {};

			virtual void OnAlive() { };

			GameObject *GetParent() { return myObject; }

			void SetEnabled(bool value);
			bool GetEnabled() const { return isEnabled; };

		protected:
			bool        isEnabled;
			GameObject *myObject;
		};
	}
}

#ifndef GAME_OBJ_H
#include "game_logic/GameObject.h"
#endif // !GAME_OBJ_H
