#pragma once

#include "GameObject.h"
#include "GameTime.h"

namespace clueless {
	namespace game_logic {

	class GameSystem {
	public:
		virtual void Init() = 0;

		virtual void PrePhysics() {};
		virtual void FixedUpdate(GameObject* object) {};
		virtual void Update(GameObject* object) {};
		virtual void LateUpdate(GameObject* object) {};

		virtual void PreRender() {};
		virtual void Render(GameObject *object) {};
		virtual void PostRender() {};

	};

} }