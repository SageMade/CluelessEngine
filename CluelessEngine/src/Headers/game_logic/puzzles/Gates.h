#pragma once
class Gates
{
public:
	Gates(bool in1, bool in2);
	bool getIn1();
	bool getIn2();
	virtual bool getOutput();

	~Gates();
private:
	bool output, in1, in2;
};

