#include <string>
#include <vector>
#include "OrGate.h"
#include "andGate.h"
#include "Switch.h"
#include "XorGate.h"
#include "Diode.h"


class Circuits {
public:

	Circuits();
	~Circuits();
	//puzzle1
	void circuit1(bool s1, bool s2, bool s3, bool s4, bool s5);
	//puzzle2
	void circuit2(bool s1, bool s2, bool s3, bool s4, bool s5);
	void circuit3(bool s1, bool s2, bool s3, bool s4);
	void circuit4(bool s1, bool s2, bool s3, bool s4, bool s5);

	void circuit5(bool sw1, bool sw2, bool sw3, bool sw4);

	//store output values of the components
	std::vector<Switch> switches;
	std::vector<andGate> and;
	std::vector<OrGate> or;
	std::vector<XorGate> xor;
	std::vector<Diode> diode;

private:
};


