#pragma once
#include "Circuits.h"
class DrawCircuit
{
public:
	DrawCircuit(int num);
	void Draw(bool in1 = false, bool in2 = false, bool in3 = false, bool in4 = false, bool in5 = false);
	
	//true if puzzle is complete
	bool complete;
	//circuit numer
	int circuitNum;
private:
	void puzzle1(bool in1, bool in2, bool in3, bool in4, bool in5);
	void puzzle2(bool in1, bool in2, bool in3, bool in4, bool in5);
	void puzzle3(bool in1, bool in2, bool in3, bool in4);
};

