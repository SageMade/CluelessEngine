#pragma once

#include <string>

#include "game_logic/GameComponent.h"
#include "game_logic/ICamera.h"
#include "graphics/CubeMap.h";
#include "graphics/Shader.h";
#include "graphics/Mesh.h";

#include "cms/CMS.h"

namespace clueless { namespace game_logic { namespace components {

	class Skybox :
		public GameComponent
	{
	public:
		Skybox() : GameComponent() {
		}
		Skybox(GameObject *parent) : GameComponent(parent) {
		}
		virtual ~Skybox() {}

		void Load();
		virtual void Render();
		void Unload();

	private:
		ContentToken              myCubeMap;
		ContentToken              myShader;
		clueless::graphics::Mesh *myGeometry;
	};


} } }