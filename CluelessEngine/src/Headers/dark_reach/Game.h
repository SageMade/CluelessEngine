#pragma once

#include <string>
#include <unordered_map>

#include "graphics/Window.h"
#include "graphics/Shader.h"
#include "graphics/Texture.h"
#include "graphics/Mesh.h"

#include "ui/input/InputSources.h"

namespace dark_reach {

	typedef std::unordered_map<std::string, clueless::ui::input::AxisInputSource*> InputAxisPool;
	typedef std::unordered_map<std::string, clueless::ui::input::ButtonInputSource*> InputButtonsPool;

	class Game {
		public:
			Game(int argc, char* args[]);
			~Game();
		
			void Init();
			void Run();

			void Stop();

		private:
			void __Init();
			void __LoadSettings();
			void __LoadContent();

			void __Update(double gameTime, double elapsedTime);
			void __Draw(double gameTime, double elapsedTime);

			bool isExiting;

			clueless::graphics::Window* myWindow;
	};

}