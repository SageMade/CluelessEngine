#ifndef STACK_H
#define STACK_H

namespace clueless {
    template <typename T>
    class Stack{
        public:
            Stack(int capacity = 32);
            ~Stack();

            void Push(const T& value);
            T Pop();
            void Resize(const int newSize); 

            T At(const int index) const;
            T Peek() const;
            int Size() const;
            bool IsFull() const;
            bool IsEmpty() const;

        private:
            int myCapacity;
            int myTopIndex;
            T*  myArray;
    };
}
#include "utils/Stack.cpp"

#endif
