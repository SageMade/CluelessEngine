#pragma once
#ifndef CURVE_H
#define CURVE_H

#include <memory>

namespace clueless {

	enum CurveMode {
		LinearCurve = 0,
		SplineCurve = 1
	};

	struct CurveKeyFrame {
		float     Time;
		float     Value;
		CurveMode Mode;

		CurveKeyFrame() {}
		CurveKeyFrame(float time, float value, CurveMode mode) : Time(time), Value(value), Mode(mode) { }
	};
	
	class Curve {
		public:
			Curve(CurveKeyFrame* frames, int length);
			float Sample(float t);

			static inline float Spline(float t, float in, float out);

		private:
			CurveKeyFrame* myFrames;
			int            myFrameCount;
	};

}

#endif