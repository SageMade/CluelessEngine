#ifndef VR_SUBSYSTEM_H
#define VR_SUBSYSTEM_H

#include <openvr/openvr.hpp>
#include <stdexcept>

#include "TrackedDevice.h"
#include "HMD.h"

namespace clueless { namespace virtual_reality {

    struct vr_exception : std::runtime_error {
        vr_exception(const char* message) : runtime_error(message) {}
    };

    class VrSubSystem {
        public:
            static void Init();
            static void Cleanup();

            static void GetRenderTargetSize(uint32_t *width, uint32_t *height);

            static void Poll();

        protected:
            static TrackedDevice *myTrackedDevices;
            static uint32_t       myDeviceCount;

            static HMD           *myHmd;

            static vr::IVRSystem *myInternalSystem;
    };

} }

#endif // VR_SUBSYSTEM_H
