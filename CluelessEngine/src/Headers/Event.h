#pragma once
#include "game_logic/GameObject.h"

enum InventoryFlags
{
	PICKEDUP = 0,
	DROPPED = 1,
	THROWN = 2
};

enum PuzzleFlags
{
	COMPLETE,
	NOTCOMPLETE
};
enum EventTypes
{
	NONE,
	InventoryTask,
	QuestType,
	AreaEntered,
	FetchTask,
	KillTask,
	RepairTask,
	ColliderEmpty,
	InventoryPickup,
	ColliderEnter,
	Puzzle
};

enum Collider
{
	Player,
	Object
};

struct Inventory
{
	Inventory() {};
	InventoryFlags flags;
	//inventory item
	clueless::game_logic::GameObject * item;
	~Inventory(){};
};

struct ColliderE
{
	InventoryFlags flags;
	
};

struct ColliderEn
{
	Collider flags;
	int id;
};


struct PuzzleStart
{
	PuzzleFlags flags;
	int id;
	
};



class Event
{
public:
	Event();
	~Event();
	EventTypes type;
	template <typename T>
	T GetData();
	void* data;


};

template <typename T>
T  Event::GetData()
{
	return *reinterpret_cast<T*>(data);
}