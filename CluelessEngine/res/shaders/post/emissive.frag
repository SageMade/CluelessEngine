#version 430

uniform sampler2D xEmissive;

uniform vec2      xTexSize;

layout(location = 0) out vec3 out_color;

void main()
{
    vec2 texUV = gl_FragCoord.xy / xTexSize;    
	vec4 in_emissive = texture(xEmissive, texUV); 
	
	out_color.rgb = in_emissive.rgb * in_emissive.a;
}
