#version 430

#define MAX_LIGHTS 8

uniform sampler2D xSampler;
uniform sampler2D xNormals;
uniform sampler2D xDepth;
uniform sampler2D xEmissive;
uniform sampler2D xSpecular;
uniform sampler2D xWorldSpace;

uniform vec2      xTexSize;

uniform mat4  xColorTransform;
uniform mat3  xKernelTransform;

uniform mat4  xView;
uniform mat4  xProjection;

// Light Settings
struct LightData {
	vec4  Position;
	vec3  Diffuse;
	float Attenuation;
	vec3  Specular;
	float Padding;
};
layout (std140) uniform xLights {
	LightData Lights[MAX_LIGHTS];
};

uniform int xMaterialId;
uniform int xNumActiveLights;
uniform vec3 xAmbientLight;

layout(location = 0) out vec4 outColor;


vec3 calcPhong(LightData light, vec3 position, vec3 norm, vec4 matSpecular);

void main() {

	vec2 texUV = gl_FragCoord.xy / xTexSize;	
	gl_FragDepth = texture(xDepth, texUV).r;

	// Re-normalize
	vec3 norm = normalize(texture(xNormals, texUV).xyz);
	vec3 position = texture(xWorldSpace, texUV).xyz;
	vec4 objColor = texture(xSampler, texUV);
	vec4 emissive = texture(xEmissive, texUV);
	vec4 specular = texture(xSpecular, texUV);

	outColor.rgb = vec3(0);

	for(int ix = 0; ix < xNumActiveLights; ix++) {
		outColor.rgb += calcPhong(Lights[ix], position, norm, specular);
	}

	outColor.rgb += xAmbientLight;
	outColor.rgb += emissive.rgb * emissive.a;
	outColor.rgb *= objColor.rgb;
	//outColor.rgb = norm;
	outColor.a = objColor.a;
}

vec3 calcPhong(LightData light, vec3 position, vec3 norm, vec4 matSpecular) {
	vec4 lightPos = xView * light.Position;
	vec3 lightVec = lightPos.xyz - position;
	float dist = length(lightVec);

	vec3 lightDir = lightVec / dist;

	float nDotL = dot(norm, lightDir);
	
	if (nDotL > 0.0) {
		// Calculate falloff (attenuation)
		float att = 1.0 / (1 + light.Attenuation * pow(dist, 2));

		// Calculate diffuse
		vec3 result = light.Diffuse * nDotL;

		// Blinn-Phong half vector
		float nDotHV = max(dot(norm, normalize(lightDir + normalize(-position))), 0.0);

		return (result + light.Specular * pow(nDotHV, matSpecular.r)) * att;
	}
}

