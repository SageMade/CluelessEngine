#version 430

uniform sampler2D xDepth;

uniform vec2      xTexSize;

layout(location = 0) out vec3 out_color;

void main()
{
    vec2 texUV = gl_FragCoord.xy / xTexSize;    
	float in_depth = texture(xDepth, texUV).r;

	const float n = 0.1f;
	const float f = 1000.0f;
	float lDepth = (2 * n) / (f + n - in_depth * (f - n));
	out_color.rgb = vec3(lDepth);
}
