#version 440

uniform sampler2D xSampler;
uniform sampler2D xNormals;
uniform sampler2D xDepth;
uniform sampler2D xEmissive;
uniform sampler2D xWorldSpace;

uniform vec2      xTexSize;

uniform mat4  xColorTransform;
uniform mat3  xKernelTransform;

uniform mat4  xView;
uniform mat4  xProjection;

layout(location = 0) out vec3 out_color;

vec4 sampleKerning3x(sampler2D sampler, vec2 texUV) {
	const float offset = 1.0 / 400.0;
	
	vec2 offsets[9] = vec2[](
        vec2(-offset,  offset), // top-left
        vec2( 0.0f,    offset), // top-center
        vec2( offset,  offset), // top-right
        vec2(-offset,  0.0f),   // center-left
        vec2( 0.0f,    0.0f),   // center-center
        vec2( offset,  0.0f),   // center-right
        vec2(-offset, -offset), // bottom-left
        vec2( 0.0f,   -offset), // bottom-center
        vec2( offset, -offset)  // bottom-right    
    );  
	vec4 out_color = vec4(0.0f);

	for(int ix = 0; ix < 9; ix ++) {
		out_color += texture(sampler, texUV + offsets[ix]) * xKernelTransform[ix / 3][ix % 3];
	}

	return out_color;
}

void main()
{
    vec2 texUV = gl_FragCoord.xy / xTexSize;    
	vec4 in_color = texture(xSampler, texUV);//sampleKerning3x(xSampler, texUV);//sampleKerning3x(xNormals, texUV);//
	vec3 in_normal = texture(xNormals, texUV).rgb; 
	float in_depth = texture(xDepth, texUV).r;

	gl_FragDepth = in_depth;

	float lightMult = length(in_normal);
		
	vec3 calc = in_color.rgb * max(0.1f, -dot(in_normal, normalize(mat3(xView) * vec3(-0.575f, -0.575f, -0.15f))));
	calc = (vec4(calc, in_color.a) * xColorTransform).rgb;
	vec4 emissive = texture(xEmissive, texUV);
	out_color.rgb = mix(in_color.rgb, calc, lightMult);
	out_color.rgb += emissive.rgb * emissive.a;
	//out_color.rgb = in_color.rgb;
}
