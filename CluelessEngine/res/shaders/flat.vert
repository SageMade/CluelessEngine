#version 410
layout (location = 0) in vec3 vertexPosition;
layout (location = 1) in vec4 vertexColor;
	
layout (location = 0) out vec4 fragmentColor;

uniform mat4 xTransform;
	
void main() {
	gl_Position = xTransform * vec4(vertexPosition, 1);
	fragmentColor = vertexColor;
}