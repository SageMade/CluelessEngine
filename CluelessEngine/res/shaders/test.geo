#version 440
		layout (points) in;
		layout (triangle_strip, max_vertices = 4) out;
		uniform mat4 xWorld;
		uniform mat4 xView;
		uniform mat4 xProjection;
		struct VsVert {
			vec4  Position;
			vec4  Color;
			float Size;
			float TexId;
		};
		struct GsVert {
			vec2  TexCoord;
			vec4  Color;
			float TexId;
		};
		in VsVert VsToGs[];

		out GsVert GsToFS;

		void main() {
			// Calculate our shader stuff (basically where in the view it is)
			mat4 MV = xView * xWorld;
			mat4 VP = xProjection * xView;
			vec3 right = vec3(MV[0][0], 
			        MV[1][0], 
			        MV[2][0]);

			vec3 up = vec3(MV[0][1], 
			        MV[1][1], 
			        MV[2][1]);

			vec3 pos = gl_in[0].gl_Position.xyz;

			vec3 tl = pos + (-right + up) * VsToGs[0].Size;
			gl_Position = VP * vec4(tl, 1.0);
			GsToFS.Color = VsToGs[0].Color;
			GsToFS.TexId = VsToGs[0].TexId;
			GsToFS.TexCoord = vec2(0.0, 0.0);

			// emit a single vertex
			EmitVertex();

			vec3 tr = pos + (right + up) * VsToGs[0].Size;
			gl_Position = VP * vec4(tr, 1.0);
			GsToFS.Color = VsToGs[0].Color;
			GsToFS.TexId = VsToGs[0].TexId;
			GsToFS.TexCoord = vec2(1.0, 0.0);

			// emit a single vertex
			EmitVertex();

			vec3 bl = pos + (-right - up) * VsToGs[0].Size;
			gl_Position = VP * vec4(bl, 1.0);
			GsToFS.Color = VsToGs[0].Color;
			GsToFS.TexId = VsToGs[0].TexId;
			GsToFS.TexCoord = vec2(0.0, 1.0);

			// emit a single vertex
			EmitVertex();

			vec3 br = pos + (right - up) * VsToGs[0].Size;
			gl_Position = VP * vec4(br, 1.0);
			GsToFS.Color = VsToGs[0].Color;
			GsToFS.TexId = VsToGs[0].TexId;
			GsToFS.TexCoord = vec2(1.0, 1.0);

			// emit a single vertex
			EmitVertex();

		}